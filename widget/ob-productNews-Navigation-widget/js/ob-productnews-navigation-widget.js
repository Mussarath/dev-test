define(

    ['knockout', 'CCi18n', 'pubsub', 'viewModels/guidedNavigationViewModel', 'storageApi'],

    function(ko, CCi18n, pubsub, GuidedNavigationViewModel, storageApi) {

        "use strict";
        var getWidget = '';
        return {
            obselectedCategory: ko.observable('products'),
            obNewsfilters: ko.observableArray([]),
            isNews: ko.observable(false),
            obnewsfilterSelected: ko.observableArray([]),
            currentpage : ko.observableArray(''),


            /*
             * Note that "this" is bound to the Widget View Model.
             */
            resourcesLoaded: function(widget) {},

            /* Method used to Swap Between Products and News which called whie clicking the Radio button */
            swapProductandNews: function(data, event) {
                var widget = getWidget;
                if(event){
                      widget.obselectedCategory(event.currentTarget.value);
                }
                
                if (widget.obselectedCategory() == "products") {
                    $.Topic("swapToProducts").publish();
                    widget.isNews(false);
                    if (document.cookie.indexOf('obnewsfiltersCookie') > -1) {
                        storageApi.getInstance().removeItem("obnewsfiltersCookie");
                    }
                } else {
                    $.Topic("swapToNews").publish();
                    if(widget.obnewsfilterSelected().length >0){
                        widget.fetchSelectedfilterdetails(widget.obnewsfilterSelected());
                    }
                    widget.isNews(true);
                   
                }
            },
            /** This Method is used for Generate the Filters statically once the widget is loaded  */
            genaratefilterforNews: function() {
                var widget = getWidget;
                widget.obNewsfilters([]);
                var filterObj, filterNewstype, filterProductCategory, filtersMonth;
                if (widget.resources().obfilterNewstype) {
                    filterNewstype = {};
                    filterNewstype.displayName = "News type";
                    filterNewstype.value = widget.resources().obfilterNewstype.indexOf(",") !== -1 ? widget.resources().obfilterNewstype.split(',') : [];
                    widget.obNewsfilters.push(filterNewstype);
                }
                if (widget.resources().obfilterProductCategory) {
                    filterProductCategory = {}
                    filterProductCategory.displayName = "Product Category";
                    filterProductCategory.value = widget.resources().obfilterProductCategory.indexOf(",") !== -1 ? widget.resources().obfilterProductCategory.split(',') : [];
                    widget.obNewsfilters.push(filterProductCategory);
                }
                if (widget.obFiltersMonth()) {
                    filtersMonth = {};
                    filtersMonth.displayName = "Month";
                    filtersMonth.value = widget.obFiltersMonth().indexOf(",") !== -1 ? widget.obFiltersMonth().split(',') : [];
                    widget.obNewsfilters.push(filtersMonth);
                }
                filterObj = {}
                filterObj.displayName = "year";
                filterObj.value = ["2020", "2019", "2018", "2017", "2016", "2015"];
                widget.obNewsfilters.push(filterObj);
            },
            /** Ends Here */
            /**This Method is used for Selection of Filter */
            obnewsSelectionMethod: function(data, event, filterName) {
                var widget = getWidget;
                var selectedNewsfilterObj = {
                    displayName: "",
                    selectedValue: []
                };
                   
                selectedNewsfilterObj.selectedValue = ko.observableArray();
                if (event.currentTarget.checked) {
                    if (widget.obnewsfilterSelected().length > 0) {
                        var newsFilterLength = widget.obnewsfilterSelected().length;
                        var a = 0;
                        while (a < newsFilterLength) {
                            if (filterName == widget.obnewsfilterSelected()[a].displayName) {
                                widget.obnewsfilterSelected()[a].selectedValue.push(data);
                            } else if (a == (newsFilterLength - 1)) {
                                selectedNewsfilterObj.displayName = filterName;
                                selectedNewsfilterObj.selectedValue.push(data);
                                widget.obnewsfilterSelected.push(selectedNewsfilterObj);
                            }
                            a++;
                        }
                    } else {
                        selectedNewsfilterObj.displayName = filterName;
                        selectedNewsfilterObj.selectedValue.push(data);
                        widget.obnewsfilterSelected.push(selectedNewsfilterObj);
                    }
                    $.Topic("obSelectedfliters").publish(widget.obnewsfilterSelected());

                } else {
                    widget.removeNewsfliter(data, filterName);
                }
            },
            /** Ends Here */
            /** Method used to remove the remove the filter once it is clicked  */
            removeNewsfliter: function(data, filterName) {
                var widget = getWidget;
                if (widget.obnewsfilterSelected().length > 0) {
                    var newsFilterLength = widget.obnewsfilterSelected().length;
                        for (var a = 0;a < newsFilterLength;a++) {
                            var removeindex = ko.toJS(widget.obnewsfilterSelected()[a].selectedValue()).indexOf(data);
                            if (filterName == widget.obnewsfilterSelected()[a].displayName && removeindex > -1) {
                                widget.obnewsfilterSelected()[a].selectedValue.splice(removeindex, 1);
                                if (widget.obnewsfilterSelected()[a].selectedValue().length == 0) {
                                    widget.obnewsfilterSelected.splice(a, 1)
                                }
                                break;
                            }
                        }
                
                }
                widget.uncheckthefliter(data);
                $.Topic("obSelectedfliters").publish(widget.obnewsfilterSelected());
            },
            /** Method used to Uncheck when we are clicking from selected filter  */
            uncheckthefliter: function(data) {
                $(".NewsFacet").each(function(i, v) {
                    if (v.innerText == data) {
                        $(v).siblings('input').prop('checked', false);
                    }
                });
            },
             /** Method used to Uncheck when we are clicking from selected filter  */
             uncheckallFilters: function(data) {
                $(".NewsFacet").each(function(i, v) {
                        $(v).siblings('input').prop('checked', false);
                });
            },
            /** This method used to for back functionality from news landing page */
            fetchSelectedfilterdetails: function(filterValues) {   
                if (filterValues.length > 0) {
                    var f = filterValues.length;
                    while (f--) {
                        if(typeof(filterValues[f].selectedValue) !=="function"){
                            filterValues[f].selectedValue = ko.observableArray(filterValues[f].selectedValue);
                        }
                        for (var z = 0; z < filterValues[f].selectedValue().length; z++) {
                            getWidget.checktheSelectedfilter(filterValues[f].selectedValue()[z]);
                        }
                    }
                }

            },
            /* Check the filter when we come from Back from News Landing page */
            checktheSelectedfilter: function(data) {
                /** Added Settimeout to wait till dom loads and it will select */
                setTimeout(function(){
                        $(".NewsFacet").each(function(i, v) {
                    if (v.innerText == data) {
                        $(v).siblings('input').prop('checked', true);
                    }
                    });
                },100);
                
            },
            onLoad: function(widget) {
                getWidget = widget;
                /** Pubsub is used to trigger the remove method */
                $.Topic("removeNewsrefinements").subscribe(function(data, filterName) {
                    widget.removeNewsfliter(data, filterName);
                });
                $.Topic("obclearAllRefinements").subscribe(function(v) {
                    widget.uncheckallFilters();
                    widget.obnewsfilterSelected([]);
                });
                /** pub sub is called when the back from News landing page */
                $.Topic("switchToNews").subscribe(function(filterValues) {
                    /** Settimeout is added to wait till Dom Loads */
                    setTimeout(function(){
                        widget.obselectedCategory("News");
                        widget.swapProductandNews();
                        widget.fetchSelectedfilterdetails(filterValues);
                    },1000);
                    
                });
                  /**  Hide when it is above the 768 width*/
				$(window).resize(function () {
					if ($(window).width() > 768) {
                        widget.handleHideRefinements();
					} 
				});
                widget.guidedNavigationViewModel = ko.observable();
                widget.guidedNavigationViewModel(new GuidedNavigationViewModel("10", "10", widget.locale()));
            },
              
            /** This Method will trigger the filter show in Mobile view */
            handleNewsRefineResults: function() {
                window.scrollTo(0, 0);
                $('#Newsoverlay').show();
                $("body").css("overflow-y", "hidden");
                $('.ob-news-refinements').addClass('addpaddigntoOverLay');
            },
            /* Method used to Hided in mobile view */
            handleHideRefinements: function() {
                if ($('body').css('overflow-y') == 'hidden'){
                    $("body").css("overflow-y", "initial");
                }
                $('.ob-news-refinements').removeClass('addpaddigntoOverLay');
                $('#Newsoverlay').hide();
            },
            
            beforeAppear: function(page) {
                var widget = this;
                widget.currentpage(page.path);
                getWidget.obselectedCategory('products');
                widget.isNews(false);
                widget.genaratefilterforNews();
                widget.obnewsfilterSelected([]);
                widget.uncheckallFilters();

            }
        }
    }
);