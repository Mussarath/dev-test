/**
 * @fileoverview osborn contact left navigation Widget.
 *
 */
define(
  
  //-------------------------------------------------------------------
 // DEPENDENCIES
 //-------------------------------------------------------------------
 
 ['jquery','knockout', 'CCi18n','pubsub'],

 function($,ko, CCi18n,pubsub) {

   "use strict";

   return {
     oB_countriesName : ko.observable(),
     oB_selectedCountry : ko.observable(),
       
     /*
      * Note that "this" is bound to the Widget View Model.
      */      
     resourcesLoaded : function(widget) {
     },

     onLoad : function(widget) { 

        widget.oB_updateLeftNav(widget);
    


     },

   /** to update countries name functionality **/
   
     oB_updateLeftNav : function(getdata){
         var self=this;
          var resources= {
               "values" : [
                 {
                   "countryreferenceid" :  getdata.resources().NAreferenceid,
                   "countryName" : getdata.resources().northamericatitletext,
                   "countryAddress1" :  getdata.resources().NAaddresstext1,
                   "countryEmail1" :  getdata.resources().NAemailtext1,
                   "countryPhoneNo1" :  getdata.resources().NAphonenotext1,
                   "countryAddress2" :  getdata.resources().NAaddresstext2,
                   "countryEmail2" :  getdata.resources().NAemailtext2,
                   "countryPhoneNo2" :  getdata.resources().NAphonenotext2,
                    "countryAddress3" :  getdata.resources().NAaddresstext3,
                   "countryEmail3" :  getdata.resources().NAemailtext3 ,
                   "countryPhoneNo3" :  getdata.resources().NAphonenotext3
                 },
                  {
                   "countryreferenceid" :  getdata.resources().SAreferenceid,
                   "countryName" : getdata.resources().southamericatitletext,
                   "countryAddress1" :  getdata.resources().SAaddresstext1 ,
                   "countryEmail1" :  getdata.resources().SAemailtext1 ,
                   "countryPhoneNo1" :  getdata.resources().SAphonenotext1,
                    "countryAddress2" :  getdata.resources().SAaddresstext2 ,
                   "countryEmail2" :  getdata.resources().SAemailtext2,
                   "countryPhoneNo2" :  getdata.resources().SAphonenotext2,
                    "countryAddress3" :  getdata.resources().SAaddresstext3,
                   "countryEmail3" :  getdata.resources().SAemailtext3,
                   "countryPhoneNo3" :  getdata.resources().SAphonenotext3
                 },
                  {
                  "countryreferenceid" :  getdata.resources().EUreferenceid, 
                   "countryName" : getdata.resources().europetitletext,
                   "countryAddress1" :  getdata.resources().EUaddresstext1 ,
                   "countryEmail1" :  getdata.resources().EUemailtext1 ,
                   "countryPhoneNo1" :  getdata.resources().EUphonenotext1,
                   "countryAddress2" :  getdata.resources().EUaddresstext2 ,
                   "countryEmail2" :  getdata.resources().EUemailtext2 ,
                   "countryPhoneNo2" :  getdata.resources().EUphonenotext2,
                   "countryAddress3" :  getdata.resources().EUaddresstext3 ,
                   "countryEmail3" :  getdata.resources().EUemailtext3 ,
                   "countryPhoneNo3" :  getdata.resources().EUphonenotext3,
                   "countryAddress4" :  getdata.resources().EUaddresstext4 ,
                   "countryEmail4" :  getdata.resources().EUemailtext4 ,
                   "countryPhoneNo4" :  getdata.resources().EUphonenotext4,
                   "countryAddress5" :  getdata.resources().EUaddresstext5 ,
                   "countryEmail5" :  getdata.resources().EUemailtext5 ,
                   "countryPhoneNo5" :  getdata.resources().EUphonenotext5,
                   "countryAddress6" :  getdata.resources().EUaddresstext6 ,
                   "countryEmail6" :  getdata.resources().EUemailtext6 ,
                   "countryPhoneNo6" :  getdata.resources().EUphonenotext6,
                   "countryAddress7" :  getdata.resources().EUaddresstext7 ,
                   "countryEmail7" :  getdata.resources().EUemailtext7 ,
                   "countryPhoneNo7" :  getdata.resources().EUphonenotext7,
                   "countryAddress8" :  getdata.resources().EUaddresstext8 ,
                   "countryEmail8" :  getdata.resources().EUemailtext8 ,
                   "countryPhoneNo8" :  getdata.resources().EUphonenotext8,
                   "countryAddress9" :  getdata.resources().EUaddresstext9 ,
                   "countryEmail9" :  getdata.resources().EUemailtext9 ,
                   "countryPhoneNo9" :  getdata.resources().EUphonenotext9
                 },
                  {
                  "countryreferenceid" :  getdata.resources().ASreferenceid,  
                   "countryName" : getdata.resources().asiatitletext,
                   "countryAddress1" :  getdata.resources().ASaddresstext1 ,
                   "countryEmail1" :  getdata.resources().ASemailtext1 ,
                   "countryPhoneNo1" :  getdata.resources().ASphonenotext1,
                   "countryAddress2" :  getdata.resources().ASaddresstext2 ,
                   "countryEmail2" :  getdata.resources().ASemailtext2 ,
                   "countryPhoneNo2" :  getdata.resources().ASphonenotext2,
                   "countryAddress3" :  getdata.resources().ASaddresstext3 ,
                   "countryEmail3" :  getdata.resources().ASemailtext3 ,
                   "countryPhoneNo3" :  getdata.resources().ASphonenotext3
                 },
                  {
                  "countryreferenceid" :  getdata.resources().AFreferenceid,  
                   "countryName" : getdata.resources().africatitletext,
                   "countryAddress1" :  getdata.resources().AFaddresstext1 ,
                   "countryEmail1" :  getdata.resources().AFemailtext1 ,
                   "countryPhoneNo1" :  getdata.resources().AFphonenotext1,
                   "countryAddress2" :  getdata.resources().AFaddresstext2 ,
                   "countryEmail2" :  getdata.resources().AFemailtext2 ,
                   "countryPhoneNo2" :  getdata.resources().AFphonenotext2,
                    "countryAddress3" :  getdata.resources().AFaddresstext3 ,
                   "countryEmail3" :  getdata.resources().AFemailtext3 ,
                   "countryPhoneNo3" :  getdata.resources().AFphonenotext3,
                 },
                  {
                  "countryreferenceid" :  getdata.resources().AUreferenceid,  
                   "countryName" : getdata.resources().australiatitletext,
                   "countryAddress1" :  getdata.resources().AUaddresstext1 ,
                   "countryEmail1" :  getdata.resources().AUemailtext1 ,
                   "countryPhoneNo1" :  getdata.resources().AUphonenotext1,
                   "countryAddress2" :  getdata.resources().AUaddresstext2 ,
                   "countryEmail2" :  getdata.resources().AUemailtext2 ,
                   "countryPhoneNo2" :  getdata.resources().AUphonenotext2,
                   "countryAddress3" :  getdata.resources().AUaddresstext3 ,
                   "countryEmail3" :  getdata.resources().AUemailtext3 ,
                   "countryPhoneNo3" :  getdata.resources().AUphonenotext3
                 },
                 /* {
                  "countryreferenceid" :  getdata.resources().ANreferenceid,  
                   "countryName" : getdata.resources().antarticatitletext,
                   "countryAddress1" :  getdata.resources().ANaddresstext1 ,
                   "countryEmail1" :  getdata.resources().ANemailtext1 ,
                   "countryPhoneNo1" :  getdata.resources().ANphonenotext1,
                   "countryAddress2" :  getdata.resources().ANaddresstext2 ,
                   "countryEmail2" :  getdata.resources().ANemailtext2 ,
                   "countryPhoneNo2" :  getdata.resources().ANphonenotext2,
                   "countryAddress3" :  getdata.resources().ANaddresstext3 ,
                   "countryEmail3" :  getdata.resources().ANemailtext3 ,
                   "countryPhoneNo3" :  getdata.resources().ANphonenotext3
                 } */
                ]
          };
          self.oB_countriesName(resources.values);
          self.oB_updateOnClick(resources.values[0]);
     },
     
   /** getting data on click and pubsub functionality **/  
   
       oB_updateOnClick : function(newData,index){
          var self=this;
          $(".names").removeClass("bgColor");
          $("#country"+index).addClass("bgColor");
          if(newData){
              self.oB_selectedCountry(newData);
              $.Topic('SELECTED_COUNTRY.memory').publish(self.oB_selectedCountry());
          }
       },
   
   

     beforeAppear : function(page) {

      //Initial Click North America
      setTimeout(function () { 
        $('#country0').click();
      },500);
    
     }
   };
 }
);
