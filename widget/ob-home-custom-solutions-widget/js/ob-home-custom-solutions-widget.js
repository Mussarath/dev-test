/**
 * @fileoverview custom solutions Carousel Widget.
 *
 * @author Mastek
 */
define(
  //--------------------------------------- ----------------------------
  // DEPENDENCIES
  //-------------------------------------------------------------------

  ['jquery', 'knockout', 'pubsub', 'notifier', 'ccConstants', 'ccRestClient', 'js/knockout.owlCarousel'],

  //-------------------------------------------------------------------
  // MODULE DEFINITION
  //-------------------------------------------------------------------
  function ($, ko, pubsub, notifier, r, ccRestClient) {

      "use strict";
      
      return {
          oB_marketingCollections:ko.observable(''),
          
          onLoad: function (widget) {
            widget.oB_updateCustomSolutions();
          },
          
        /* marketing carousel data functionality */
        oB_updateCustomSolutions : function(){
                var self=this,catalogId,siteId,l={};
                l['fields'] = 'childCategories';
                l['expand'] = 'childCategories';
                l['maxlevel'] = 1000;
                siteId = self.site().extensionSiteSettings.externalSiteSettings.langCode;
                if((siteId === "en-US") || (siteId === "es")){
                  catalogId = self.resources().catalogUS ;
                }else{
                  catalogId = self.resources().catalogAll;
                }
                ccRestClient.authenticatedRequest("/ccstoreui/v1/collections/" + catalogId ,l, function (data) {
                    if(data){
                         self.oB_marketingCollections(data.childCategories);
                    }
                }, function (err) {
                }, "GET");   
        },
      /* ends here */
        
        // finds the news and events div heights
        
        oB_resizeNewsEventsHeight: function() {
          var highestBox = 0;
          $('#owlCarousel .owl-item .cc-product-title h4').each(function(){  
              if($(this).height() > highestBox){  
                  highestBox = $(this).height();  
              }
          });    
          $('#owlCarousel .owl-item .cc-product-title h4').height(highestBox);
              
        },
      
        beforeAppear: function (page) {
          var widget = this;
          // checks the container is visible on the page 
           function checkContainer () {
              if($('#owlCarousel .owl-item .cc-product-title').is(':visible')){  
                var evt = window.document.createEvent('UIEvents'); 
                evt.initUIEvent('resize', true, false, window, 0); 
                window.dispatchEvent(evt);
                
                widget.oB_resizeNewsEventsHeight();
                
              } else {
                setTimeout(checkContainer,300); /** wait 300 ms, then try again **/
              }
            }
            // function available after the document is loaded. 
            $(document).ready(checkContainer);
            
            // On window resizing this function will work 
            $( window ).resize(function() {
                widget.oB_resizeNewsEventsHeight();
              });
          },
          
      };
  }
);