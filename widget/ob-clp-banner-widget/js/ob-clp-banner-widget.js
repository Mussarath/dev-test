/**
 * @fileoverview clp banner Widget.
 *
 * @author Mastek
 */
define(
   //--------------------------------------- ----------------------------
  // DEPENDENCIES
  //-------------------------------------------------------------------
  ['jquery','knockout', 'CCi18n','ccRestClient'],

  function($,ko, CCi18n,ccRestClient) {

    "use strict";

    return {
        oB_clpCollections : ko.observable(''),
      /*
       * Note that "this" is bound to the Widget View Model.
       */      
      resourcesLoaded : function(widget) {
      },

      onLoad : function(widget) { 
       widget.oB_updateBannerContent();
      },


       /* clp banner data functionality */
        oB_updateBannerContent : function(){
                var self=this;
                var collectionID = 'obProducts';
                if(self.locale() === "en_US" || self.locale() === "es"){
                  collectionID = 'USOBProducts';
                }
                ccRestClient.authenticatedRequest("/ccstoreui/v1/collections/"+collectionID,{}, function (data) {
                    if(data){    
                        self.oB_clpCollections(data);
                    }
                }, function (err) {
                }, "GET");   
        },
       /* ends here */

      beforeAppear : function(page) {
        /** Full width banner **/
        $("#clpBanner").parents('#page').addClass('removeContainer');
      }
    };
  }
);
