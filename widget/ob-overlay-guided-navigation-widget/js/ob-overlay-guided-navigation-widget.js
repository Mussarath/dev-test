/**
 * @fileoverview Overlayed Guided Navigation Widget. 
 * 
 */
define(
 
  //-------------------------------------------------------------------
  // DEPENDENCIES
  //-------------------------------------------------------------------
  ['knockout','viewModels/guidedNavigationViewModel', 'CCi18n',
  'ccConstants', 'pubsub'],

  //-------------------------------------------------------------------
  // MODULE DEFINITION
  //-------------------------------------------------------------------
  function(ko, GuidedNavigationViewModel, CCi18n, CCConstants, pubsub) {  
  
    "use strict";
  
    return {

      /**
        Overlayed Guided Navigation Widget.
        @private
        @name guided-navigation
        @property {observable GuidedNavigationViewModel} view model object representing the guided navigation details
       */
      onLoad : function(widget) {
        widget.guidedNavigationViewModel = ko.observable();
        widget.guidedNavigationViewModel(new GuidedNavigationViewModel(widget.maxDimensionCount(), widget.maxRefinementCount(), widget.locale()));
        widget.isExpanded = ko.observable(false);
        widget.isNavigationVisible = ko.observable(false);
        widget.hideRefinements = function() {
          widget.isNavigationVisible(false);
          $('html, body').css('overflow-y', 'initial');
        
          if ($('#CC-overlayedGuidedNavigation').hasClass('CC-overlayedGuidedNavigation-mobileView')) {
            $('#CC-overlayedGuidedNavigation').removeClass('CC-overlayedGuidedNavigation-mobileView');
          }
          if ( widget.guidedNavigationViewModel().dimensions() && widget.guidedNavigationViewModel().dimensions().length > 0 ) {
            for (var i = 0; i < widget.guidedNavigationViewModel().dimensions().length; i++) {
              if (widget.guidedNavigationViewModel().dimensions()[i].shouldDimensionExpand()) {
                widget.guidedNavigationViewModel().dimensions()[i].shouldDimensionExpand(true);
              }
            }
          }
        };
         /**  Hide when it is above the 768 width*/
				$(window).resize(function () {
					if ($(window).width() > 768) {
            widget.hideRefinements();
					} 
				});
        widget.openNavigation = function() {
          widget.isNavigationVisible(true);
        };
        $.Topic(pubsub.topicNames.OVERLAYED_GUIDEDNAVIGATION_HIDE).subscribe(widget.hideRefinements);
        $.Topic(pubsub.topicNames.OVERLAYED_GUIDEDNAVIGATION_SHOW).subscribe(widget.openNavigation);
        /** Logic to show push menu when user is in overlay guided navigation **/
				$("body").delegate(".mobile-navbar-link", "touchstart", function () {
          widget.handleHideRefinements();
        });
      },
      
      /**
       * Handles click on Done button .It hides the overlayed guided navigation.
       */
      handleHideRefinements : function(data, event) {
        this.hideRefinements();
        $('#CC-productList-refineResults').focus();
        $('body').find('.ob-header-widget_1').css('z-index','1');
        return false;
      },
      
      /**
       * Handles click on the collapsible dimensions list.
       */
      collapseDimension : function(data, event) {
        if (!data.isExpanded()) {
          data.isExpanded(true);
          data.ariaLabelText('collapseText');
        } else {
          data.isExpanded(false);
          data.ariaLabelText('expandText');
        }
      }
    };
    
  }
);
