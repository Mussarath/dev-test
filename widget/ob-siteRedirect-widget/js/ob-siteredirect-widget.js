define(

    ['knockout', 'jquery', 'CCi18n', 'navigation', 'storageApi'],

    function(ko, $, CCi18n, n, storageApi) {


        "use strict";
        var getwidget = '';
        return {

            /*
             * Note that "this" is bound to the Widget View Model.
             */
            resourcesLoaded: function(widget) {

            },
            getParameterByName: function(name, url) {
                if (!url) url = window.location.href;
                name = name.replace(/[\[\]]/g, "\\$&");
                var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                    results = regex.exec(url);
                if (!results) return null;
                if (!results[2]) return '';
                return decodeURIComponent(results[2].replace(/\+/g, " "));
            },
            addReplaceLangCode: function(url, langCode) {
                var paths = [];
                var a = document.createElement('a');
                a.href = document.location.href;
                paths = a.pathname.split('/') ? a.pathname.split('/') : []; 
                console.log("paths===>",JSON.stringify(paths),"paths Length",paths.length);
                if(paths.length != 0){
                    console.log("came in",paths[0] != "");
                    paths.shift();
                    if(paths[0]){
                        if (paths[0].length == 2 || paths[0].length === 5) {
                            paths[0] = langCode;
                        }  else {
                            paths.unshift(langCode);
                        }
                    } else {
                        paths.unshift(langCode);
                    }
                }else{
                    paths.push(langCode);
                }

                return a.protocol + '//' +
                    a.host + '/' + paths.join('/') +
                    (a.search !== '' ? a.search : '') +
                    (a.hash !== '' ? a.hash : '');
            },
            getUserLocation: function() {

                var ipStackURL = getwidget.site().extensionSiteSettings.externalSiteSettings.ipStackUrl;
                $.ajax({
                    url: ipStackURL,
                    success: function(json) {
                        var obsitecookie = {
                            userdefault: "",
                            userselected: ""
                        };
                        var redirectSite = window.location.pathname.split('/').length > 1 ? window.location.pathname.split('/')[1] : '';
                        var url = "";
                        console.log("JSON",json);
                        if (json.country_code !== null) {

                            if (json.country_code === "US") {
                                // redirect to US english
                                obsitecookie.userdefault = 'en-US';
                                // set ob-user-site with user-default with 30 days expiration
                                storageApi.getInstance().saveToCookies("obsitecookie", JSON.stringify(obsitecookie), 30);
                                url = getwidget.addReplaceLangCode(window.location.href, 'en-US');
                            } else if(json.country_code === "FR") {
                                // redirect to FR
                                obsitecookie.userdefault = 'fr';
                                // set ob-user-site with user-default with 30 days expiration
                                storageApi.getInstance().saveToCookies("obsitecookie", JSON.stringify(obsitecookie), 30);
                                url = getwidget.addReplaceLangCode(window.location.href, 'fr');
                            }else if(json.country_code === "SE") {
                                 // redirect to SV
                                // set ob-user-site with user-default with 30 days expiration
                                obsitecookie.userdefault = 'sv';
                                storageApi.getInstance().saveToCookies("obsitecookie", JSON.stringify(obsitecookie), 30);
                                url = getwidget.addReplaceLangCode(window.location.href, 'sv');
    
                            }else if(json.country_code === "DE") {
                                 // redirect to DE
                                // set ob-user-site with user-default with 30 days expiration
                                obsitecookie.userdefault = 'de';
                                storageApi.getInstance().saveToCookies("obsitecookie", JSON.stringify(obsitecookie), 30);
                                url = getwidget.addReplaceLangCode(window.location.href, 'de');
    
                            }else if(json.country_code === "ES") {
                                 // redirect to ES
                                obsitecookie.userdefault = 'es';
                                storageApi.getInstance().saveToCookies("obsitecookie", JSON.stringify(obsitecookie), 30);
                                url = getwidget.addReplaceLangCode(window.location.href, 'es');
    
                            }else{
                                 //if country code not exists
                                // set ob-user-site with user-default with 30 days expiration
                                obsitecookie.userdefault = 'en';
                                storageApi.getInstance().saveToCookies("obsitecookie", JSON.stringify(obsitecookie), 30);
                                url = getwidget.addReplaceLangCode(window.location.href, 'en');
                            }
                        } 
                        if (obsitecookie.userdefault !== redirectSite) {
                            window.location.href = url;
                        }
                    }
                });



            },
            
            isJson : function(str){
                 try {
                    JSON.parse(str);
                } catch (e) {
                    return false;
                }
                return true;
            },
            onLoad: function(widget) {
                getwidget = widget;

                var obsitecookie = '';
                obsitecookie = storageApi.getInstance().readFromCookies("obsitecookie");
                if(window.location.pathname.indexOf('sitemap') === -1 || window.location.pathname.indexOf('robot') === -1){
                if (obsitecookie) {
                    if(widget.isJson(obsitecookie)){
                        var url = "";
                        obsitecookie = JSON.parse(obsitecookie);
                        var userPreferred = "";
                        var redirectSite = window.location.pathname.split('/').length > 1 ? window.location.pathname.split('/')[1] : '';
                        
                            //if user site cookie exists
                            if (obsitecookie.userselected) {
                                url = getwidget.addReplaceLangCode(window.location.href, obsitecookie.userselected);
                                // redirect to user selected region
                                userPreferred = obsitecookie.userselected;
                            } else {
                                // redirect user default
                                url = getwidget.addReplaceLangCode(window.location.href, obsitecookie.userdefault);
                                userPreferred = obsitecookie.userdefault;
                            }
    
                            if (userPreferred !== redirectSite) {
                                window.location.href = url;

                            }
                        
                    }else{
                    
                        // removing the cookie if there is json cant been parsed
                         storageApi.getInstance().removeItem("obsitecookie");     
                        widget.getUserLocation();
                    }
                    
                } else {
                   
                    //if user comes for 1st time to site 
                    widget.getUserLocation();
                }
            }
                

            },

            beforeAppear: function(page) {
                
            }
        }
    }
);