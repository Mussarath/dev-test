define(

    ['knockout', 'CCi18n', 'viewModels/guidedNavigationViewModel','storageApi'],
  
    function(ko, CCi18n, GuidedNavigationViewModel,storageApi) {
  
        "use strict";
  
        return {
            obSelectedNewsfilters: ko.observableArray([]),
            isNews: ko.observable(false),
            isProducts: ko.observable(false),
            resourcesLoaded: function(widget) {},
            obclearallrefinements:function(){
                var widget =this;
                 // removing the cookie if it is not news page
                 if(document.cookie.indexOf('obnewsfiltersCookie') > -1){
                    storageApi.getInstance().removeItem("obnewsfiltersCookie");
                  }
                  
                widget.obSelectedNewsfilters([])
              $.Topic("obclearAllRefinements").publish(true);
            },
            onLoad: function(widget) {
                widget.guidedNavigationViewModel = ko.observable();
                widget.guidedNavigationViewModel(new GuidedNavigationViewModel("10", "10", widget.locale()));
                /** Method is called when filter is selected */
                $.Topic("obSelectedfliters").subscribe(function(flitervalues) {
                   
                    widget.obSelectedNewsfilters(flitervalues);
                });
                /** Method is called when swap from products to news */
                $.Topic("swapToNews").subscribe(function() {
                    if(widget.pageContext().pageType.id === 'noSearchResultsPageType'){
                        widget.isProducts(true);
                        widget.isNews(false);
                   }
                   else{
                    widget.isNews(true);
                    widget.isProducts(false);  
                   }                                         
                });
                /** Method is called when swap from News to Products */
                $.Topic("swapToProducts").subscribe(function() {
                    widget.isNews(false);
                     if(widget.pageContext().pageType.id === 'noSearchResultsPageType'){
                          widget.isProducts(true);
                     }
                });
                /** Method is called when coming back from News landing page */
                $.Topic("switchToNews").subscribe(function(flitervalues) {
                    widget.obSelectedNewsfilters(flitervalues);
                    widget.isNews(true);
                });
                
                /** Method is called when there is news results */
                 $.Topic("newsResults.memory").subscribe(function() {
                        widget.isProducts(false);
                        widget.isNews(true);
                });
  
            },
            /** Method used to trigger when when filter is removed  */
            obNewsremoveRefinement: function(data, filterName) {
                // removing the cookie if it is not news page
                if(document.cookie.indexOf('obnewsfiltersCookie') > -1){
                  storageApi.getInstance().removeItem("obnewsfiltersCookie");
                }
                
                $.Topic("removeNewsrefinements").publish(data, filterName);
            },
            beforeAppear: function(page) {
                var widget = this;
                widget.isNews(false);
                widget.obSelectedNewsfilters([]);
                if(widget.pageContext().pageType.id === 'noSearchResultsPageType'){
                widget.isProducts(true);
                }
                else{
                    widget.isProducts(false);
                }
            }
        }
    }
  );