/**
 * @fileoverview osborn contact Widget.
 *
 */
define(
   
  //-------------------------------------------------------------------
 // DEPENDENCIES
 //-------------------------------------------------------------------
 
 ['knockout', 'CCi18n','pubsub', 'ccRestClient', 'spinner'],

 function(ko, CCi18n,pubsub,ccRestClient, spinner) {

   "use strict";

   return {
       oB_contactInformation : ko.observable(),
       ob_contactDetails : ko.observable({}),
       obContactRule: ko.observable({}),
       oB_showMessage : ko.observable(false),
       oB_hideMessage : ko.observable(false),
       oB_currentISDnumber : ko.observable(''),
       //oB_numeric: ko.observable(),
     /*
      * Note that "this" is bound to the Widget View Model.
      */      
      
     resourcesLoaded : function(widget) {
     },
     
     /**
                 * Destroy the 'loading' spinner.
                 * @function  OrderViewModel.destroySpinner   
                 */
                destroySpinner: function() {
                    $('#loadingModal').hide();
                    spinner.destroy();
                },
    
            /**
             * Create the 'loading' spinner.
             * @function  OrderViewModel.createSpinner
             */
        createSpinner: function(loadingText) {
            var indicatorOptions = {
                parent: '#loadingModal',
                posTop: '0',
                posLeft: '50%'
            };
            var loadingText = CCi18n.t('ns.common:resources.loadingText');
            $('#loadingModal').removeClass('hide');
            $('#loadingModal').show();
            indicatorOptions.loadingText = loadingText;
            spinner.create(indicatorOptions);
        },

     onLoad : function(widget) {
      widget.ob_contactDetails(new widget.ob_createContactDetails());
     
      /** fix for ie/edge/firefox to accept only numbers in input box **/
      ko.bindingHandlers.oB_numeric = {
        init: function (element, valueAccessor) {
            $(element).on("keydown", function (event) {
                // Allow: backspace, delete, tab, escape, and enter
                if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
                    // Allow: Ctrl+A
                    (event.keyCode == 65 && event.ctrlKey === true) ||
                    // Allow: . ,
                    (event.keyCode == 188 || event.keyCode == 190 || event.keyCode == 110) ||
                    // Allow: home, end, left, right
                    (event.keyCode >= 35 && event.keyCode <= 39)) {
                    // let it happen, don't do anything
                    return;
                }
                else {
                    // Ensure that it is a number and stop the keypress
                    if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                        event.preventDefault();
                    }
                }
            });
        }
    };


      $.Topic('SELECTED_COUNTRY.memory').subscribe(function(datanew) {
        widget.oB_updateInformation(datanew); 
      });
         
      /** start validation rules for contact */
      
      ko.validation.registerExtenders();

      widget.obContactRule().isModified = function () {
        return( widget.ob_contactDetails().ob_contactName().isModified() || 
                widget.ob_contactDetails().ob_contactCompany().isModified() || 
                widget.ob_contactDetails().ob_contactIndustry().isModified() || 
                widget.ob_contactDetails().ob_contactEmail().isModified() || 
                widget.ob_contactDetails().ob_contactPhone().isModified() || 
                widget.ob_contactDetails().ob_contactSubject().isModified() || 
                widget.ob_contactDetails().ob_contactMessage().isModified()
        );
      };

      widget.obContactRule().isValid = function () {
        return( widget.ob_contactDetails().ob_contactName.isValid() && 
                widget.ob_contactDetails().ob_contactCompany.isValid() && 
                widget.ob_contactDetails().ob_contactIndustry.isValid() && 
                widget.ob_contactDetails().ob_contactEmail.isValid() && 
                widget.ob_contactDetails().ob_contactPhone.isValid() && 
                widget.ob_contactDetails().ob_contactSubject.isValid() && 
                widget.ob_contactDetails().ob_contactMessage.isValid()

        );
      };

      widget.obContactRule().validateNow = function () {
        widget.ob_contactDetails().ob_contactName.isModified(true);
        widget.ob_contactDetails().ob_contactCompany.isModified(true);
        widget.ob_contactDetails().ob_contactIndustry.isModified(true);
        widget.ob_contactDetails().ob_contactEmail.isModified(true);
        widget.ob_contactDetails().ob_contactPhone.isModified(true);
        widget.ob_contactDetails().ob_contactSubject.isModified(true);
        widget.ob_contactDetails().ob_contactMessage.isModified(true);
      
        return(widget.obContactRule().isValid());
      };

      widget.obContactRule().reset = function() {
        widget.ob_contactDetails().ob_contactName('');
        widget.ob_contactDetails().ob_contactCompany('');
        widget.ob_contactDetails().ob_contactIndustry('');
        widget.ob_contactDetails().ob_contactEmail('');
        widget.ob_contactDetails().ob_contactPhone('');
        widget.ob_contactDetails().ob_contactSubject('');
        widget.ob_contactDetails().ob_contactMessage('');
      };

      widget.obContactRule().resetModified = function() {
        widget.ob_contactDetails().ob_contactName.isModified(false);
        widget.ob_contactDetails().ob_contactCompany.isModified(false);
        widget.ob_contactDetails().ob_contactIndustry.isModified(false);
        widget.ob_contactDetails().ob_contactEmail.isModified(false);
        widget.ob_contactDetails().ob_contactPhone.isModified(false);
        widget.ob_contactDetails().ob_contactSubject.isModified(false);
        widget.ob_contactDetails().ob_contactMessage.isModified(false);
      };

      widget.ob_contactDetails().ob_contactName.extend({
        required: {
          params: true,
          message: CCi18n.t('ns.ob-contact-widget_1:resources.errorNameRequired')
        },
        minLength: {
          params: 2,
          message: CCi18n.t('ns.ob-contact-widget_1:resources.errorNameMin', {length: 2})
        },
        maxLength: {
          params: 50,
          message: CCi18n.t('ns.ob-contact-widget_1:resources.errorNameMax', {length: 50})
        },
        pattern: {
         params: '^[^0-9]*$', /** All Characters without numbers*/
          message: CCi18n.t('ns.ob-contact-widget_1:resources.errorNameCharacter')
        }
      });
      

      widget.ob_contactDetails().ob_contactCompany.extend({
        required: {
          params: true,
          message: CCi18n.t('ns.ob-contact-widget_1:resources.errorCompanyRequired')
        },
        minLength: {
          params: 2,
          message: CCi18n.t('ns.ob-contact-widget_1:resources.errorCompanyMin', {length: 2})
        },
        maxLength: {
          params: 50,
          message: CCi18n.t('ns.ob-contact-widget_1:resources.errorCompanyMax', {length: 50})
        }
      });

      widget.ob_contactDetails().ob_contactIndustry.extend({
        required: {
          params: true,
          message: CCi18n.t('ns.ob-contact-widget_1:resources.errorIndustryRequired')
        },
        minLength: {
          params: 2,
          message: CCi18n.t('ns.ob-contact-widget_1:resources.errorIndustryMin', {length: 2})
        },
        maxLength: {
          params: 50,
          message: CCi18n.t('ns.ob-contact-widget_1:resources.errorIndustryMax', {length: 50})
        }
      });

      widget.ob_contactDetails().ob_contactEmail.extend({
        required: {
          params: true,
          message: CCi18n.t('ns.ob-contact-widget_1:resources.errorEmailRequired')
        },
        minLength: {
          params: 8,
          message: CCi18n.t('ns.ob-contact-widget_1:resources.errorEmailMin', {length: 8})
        },
        maxLength: {
          params: 50,
          message: CCi18n.t('ns.ob-contact-widget_1:resources.errorEmailMax', {length: 50})
        },
        email: {
          params: true,
          message: CCi18n.t('ns.ob-contact-widget_1:resources.errorEmailCharacter')
        }
      });

      widget.ob_contactDetails().ob_contactPhone.extend({
        required: {
          params: true,
          message: CCi18n.t('ns.ob-contact-widget_1:resources.errorPhoneRequired')
        },
        minLength: {
          params: 5,
          message: CCi18n.t('ns.ob-contact-widget_1:resources.errorPhoneMin', {length: 5})
        },
        maxLength: {
          params: 15,
          message: CCi18n.t('ns.ob-contact-widget_1:resources.errorPhoneMax', {length: 15})
        },
        pattern: {
          params: '^[0-9]*$', /** numbers only */
          message: CCi18n.t('ns.ob-contact-widget_1:resources.errorPhoneCharacter')
        }
      });

      widget.ob_contactDetails().ob_contactSubject.extend({
        required: {
          params: true,
          message: CCi18n.t('ns.ob-contact-widget_1:resources.errorSubjectRequired')
        },
        minLength: {
          params: 5,
          message: CCi18n.t('ns.ob-contact-widget_1:resources.errorSubjectMin', {length: 5})
        },
        maxLength: {
          params: 50,
          message: CCi18n.t('ns.ob-contact-widget_1:resources.errorSubjectMax', {length: 50})
        }
      });

      widget.ob_contactDetails().ob_contactMessage.extend({
        required: {
          params: true,
          message: CCi18n.t('ns.ob-contact-widget_1:resources.errorMessageRequired')
        },
        minLength: {
          params: 5,
          message: CCi18n.t('ns.ob-contact-widget_1:resources.errorMessageMin', {length: 5})
        },
        maxLength: {
          params: 250,
          message: CCi18n.t('ns.ob-contact-widget_1:resources.errorMessageMax', {length: 250})
        }
      });

      /** End validation rules for contact */
     },
     
    

    /**   contact information  data functionality  **/
     oB_updateInformation : function(name){
        var self =this; 
        self.oB_contactInformation(name);
     },
     

     beforeAppear : function(page) {
         var widget=this;
         //Initially Hide the Success and Error
         widget.oB_showMessage(false);
         widget.oB_hideMessage(false);
         	 /** default value for select box corresponding to site**/
            switch(widget.locale()){
              case "en":
                     $('select option:contains("United States (+1)")').prop('selected',true);
                     break;
              case "en_US":
                     $('select option:contains("United States (+1)")').prop('selected',true);
                     break; 
              case "de":
                  $('select option:contains("Germany (+49)")').prop('selected',true);
                  break;   
              case "es":
                  $('select option:contains("Spain (+34)")').prop('selected',true);
                  break;      
              case "fr":
                  $('select option:contains("France (+33)")').prop('selected',true);
                  break;  
              case "sv":
                  $('select option:contains("Sweden (+46)")').prop('selected',true);
                  break;      
             }  
     },

     /** construct contact details */
     ob_createContactDetails: function() {
      this.ob_contactName = ko.observable('');
      this.ob_contactCompany = ko.observable('');
      this.ob_contactIndustry = ko.observable('');
      this.ob_contactEmail = ko.observable('');
      this.ob_contactPhone = ko.observable('');
      this.ob_contactSubject = ko.observable('');
      this.ob_contactMessage = ko.observable('');
    },
    /** reset contact form  */
    ob_resetContactDetails: function() {
      var widget = this;
      widget.ob_contactDetails().ob_contactName('');
      widget.ob_contactDetails().ob_contactCompany('');
      widget.ob_contactDetails().ob_contactIndustry('');
      widget.ob_contactDetails().ob_contactEmail('');
      widget.ob_contactDetails().ob_contactPhone('');
      widget.ob_contactDetails().ob_contactSubject('');
      widget.ob_contactDetails().ob_contactMessage('');
    },

      
            
        /** change event for select box  **/
        updateISDnumber: function(data, event) {
        var widget = this;
        var getValue = $(event.currentTarget).val();
        if(getValue){
          widget.oB_currentISDnumber(getValue);
        };
        },

    /** submit contact form */
    ob_contactSubmit: function() {
      var widget = this;
      widget.obContactRule().validateNow();
   
      if(!widget.obContactRule().isValid()) {
        return false;
      }
      if(widget.obContactRule().isValid()){
          widget.createSpinner();
          var contactAPI = widget.site().extensionSiteSettings.externalSiteSettings.emailSSEurl ; /*** will change it to External site settings */
          var obContactData = {};
          obContactData.templateId = "contactUsEmail";
          obContactData.data = {};

          /** company name and address from dealer locator **/
          var companyName,companyAddress,urldata,redefined;
          urldata = window.location.search;
          if(urldata){
            if((urldata.indexOf("&") !== -1) && (urldata.indexOf("=") !== -1)){
              redefined = decodeURIComponent(urldata).split('&&');
              companyName = redefined[0].split('=');
              companyAddress=  redefined[1].split('=');  
            }
          } 
          
          
          var obJSONdata = {};
          obJSONdata.obName = widget.ob_contactDetails().ob_contactName();
          obJSONdata.obCompany = widget.ob_contactDetails().ob_contactCompany();
          obJSONdata.obIndustry = widget.ob_contactDetails().ob_contactIndustry();
          obJSONdata.obEmail = widget.ob_contactDetails().ob_contactEmail();
          obJSONdata.obPhone = widget.oB_currentISDnumber() + widget.ob_contactDetails().ob_contactPhone();
          obJSONdata.obSubject = widget.ob_contactDetails().ob_contactSubject();
          obJSONdata.obMessage = widget.ob_contactDetails().ob_contactMessage();
          if(companyAddress && companyName){
            obJSONdata.obCompanyName = companyName[1];
            obJSONdata.obCompanyAddress = companyAddress[1]; 
          }else{
            obJSONdata.obCompanyName = "";
            obJSONdata.obCompanyAddress = ""; 
          }
          obJSONdata.obRegion = widget.site().extensionSiteSettings.externalSiteSettings.langCode;
    
          obJSONdata.obHost = location.protocol+'//'+location.hostname;
          obJSONdata.obToSystemEmail = $("#obToSystemEmail").val();    
         
          obContactData.data = obJSONdata;
         
          ccRestClient.authenticatedRequest(contactAPI,obContactData, function (data) {
             //Show success Message
             if(data) {
               widget.oB_showMessage(true);
               $(window).scrollTop(0);
               $("#success-close").click(function(){
                    widget.oB_showMessage(false);
                });  
               setTimeout(function() {
                  widget.oB_showMessage(false);
                }, 60000);
             }
             widget.destroySpinner();
          }, function (err) {
             //Show error Message
             widget.oB_hideMessage(true);
             widget.destroySpinner();
             $(window).scrollTop(0);
             $("#failure-close").click(function(){
               widget.oB_hideMessage(false);
            });
             setTimeout(function() {
               widget.oB_hideMessage(false);
             }, 60000);
          }, "POST");
         
        widget.ob_resetContactDetails();
        widget.obContactRule().resetModified();
      }
    }


   };
 }
);
