/** fileoverview collection left navigation  **/
define(

  ['knockout', 'CCi18n', 'ccRestClient', 'navigation', 'pubsub'],

  function(ko, CCi18n ,ccRestClient , n, pubsub) {

    "use strict";

    return {
        ob_collections: ko.observable(),
        ob_selectedCategory: ko.observable(),
      resourcesLoaded : function(widget) {
      },

      onLoad : function(widget) { 
      },
      
      //Get the Categories list
      ob_getCategory: function(widget){
          var widget = this;
            var getURL = window.location.pathname;
          if(getURL.split('/').length>3){
            var getProductID = getURL.split('/')[4];
          }
          widget.ob_selectedCategory(getProductID);
          var getCategoryPath = widget.category().categoryIdPaths[0];
          if(getCategoryPath.split('>').length>0){
              var getColletionID = getCategoryPath.split('>')[1];
          }
           var l = {};
           l['expand'] = 'childCategories';
        l['fields'] = 'childCategories';
        l['maxLevel'] = 1000;
        ccRestClient.authenticatedRequest("/ccstoreui/v1/collections/"+getColletionID,l, function (data) {
          if(data){
             widget.ob_collections(data);
             $.Topic('getCategory.memory').publish(data);
            }
            }, function (err) {
        }, "GET");
      },
      
       beforeAppear : function(page) {
          var widget = this;
          //Get Categories call
          widget.ob_getCategory();
      },          
    }
  }
);