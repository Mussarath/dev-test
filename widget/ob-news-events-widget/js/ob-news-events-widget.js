/**
 * @fileoverview osborn News and Events Widget.
 *
 */
define(
	//-------------------------------------------------------------------
	// DEPENDENCIES
	//-------------------------------------------------------------------

	['jquery', 'knockout', 'CCi18n', 'ccRestClient', 'navigation'],

	function ($, ko, CCi18n, ccRestClient, n) {

		"use strict";
		var getwidget = '';
		var ajaxRequests = [];
		return {
			ob_newsContent: ko.observable(''),
			/*
			 * Note that "this" is bound to the Widget View Model.
			 */
			resourcesLoaded: function (widget) {},

			onLoad: function (widget) {
				getwidget = widget;
				widget.ob_updateContent();
			},	
			/** asynchronous call  to  get news and events data  **/
			ob_updateContent: function () {
				var widget = this, items,getDataUrl;
				var channelToken = widget.site().extensionSiteSettings.externalSiteSettings.channelToken;
				var url = widget.site().extensionSiteSettings.externalSiteSettings.OCECServerURL;
				if(widget.contentItemId().indexOf(',')){
					items = widget.contentItemId().split(',');
					ajaxRequests =[];
				}else{
					items = [];
				}
				for (var i = 0; i < items.length; i++) {
					getDataUrl = url + '/content/published/api/v1.1/items/'+items[i]+'?channelToken='+channelToken;
					widget.getItemDetails(getDataUrl);
				}
				
			},
			getItemDetails : function (itemurl){
				var channelToken = getwidget.site().extensionSiteSettings.externalSiteSettings.channelToken;
				var url = getwidget.site().extensionSiteSettings.externalSiteSettings.OCECServerURL;
				function configCall(data) {
					var def = $.Deferred();
					var ajaxRequestObj = [];
					if (data) {
						ajaxRequestObj = $.ajax({
							url: data,
							method: "GET",
							dataType: 'json',
						});
					}
					ajaxRequests.push(ajaxRequestObj);
					$.when.apply($, ajaxRequests).done(function () {
						def.resolve(arguments);
					});
					return def.promise();
				}
				configCall(itemurl).done(function (response) {
					var newValues = [];
					if (response.length > 0) {
						for (var z = 0; z < response.length; z++) {
							if (typeof response[z][0] === 'object') {
								newValues.push(response[z][0]);
							}
						}
						if (newValues.length > 0) {
							var fieldValue = [];
							for (var l = 0; l < newValues.length; l++) {
								if(newValues[l].fields){
									if(newValues[l].id){
									  newValues[l].fields['content_Id'] = newValues[l].id;
									}
									else{
									 newValues[l].fields['content_Id'] = "";   
									}
									fieldValue.push(newValues[l].fields);
								}
							}
							/** setup actual data **/

							if (fieldValue.length > 0) {
								var image; 
								for (var k = 0; k < fieldValue.length; k++) {
									if(fieldValue[k].news_image){
										image = url + '/content/published/api/v1.1/assets/' + fieldValue[k].news_image.id + '/Large/' + fieldValue[k].news_image.name + '?format=jpg&type=responsiveimage&channelToken='+ channelToken;
										fieldValue[k].news_image['primaryImage'] = image;
									}else {
										image = {'primaryImage':'file/general/no-image.png'};
										fieldValue[k].news_image = image;
									}
								}
								getwidget.ob_newsContent(fieldValue);

							}
						}

					}

				});

			},

			/** News Landing Page redirection and url parameter **/
			ob_redirectNewsLanding: function (data) {
				$(window).scrollTop(0);
				var url = "/obNewsLandingPage" + "?" + "Id=" + data.content_Id;
				n.goTo(url);
			},

			beforeAppear: function (page) {
				var widget = this;
			}
		};
	}
);