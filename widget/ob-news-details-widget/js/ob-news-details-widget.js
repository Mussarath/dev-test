/**
 * @fileoverview osborn News Details Widget.
 *
 */
define(
  //-------------------------------------------------------------------
// DEPENDENCIES
//-------------------------------------------------------------------    
  
['knockout', 'CCi18n','spinner','navigation'],
function(ko, CCi18n,spinner,n) {
  "use strict";
  return {
      ob_newsdetails: ko.observable(),
      ob_newsTitle: ko.observable(""),
      ob_imageData: ko.observable(""),
      
    resourcesLoaded : function(widget) {
    },
    
      /**
       * Destroy the 'loading' spinner.
       * @function  OrderViewModel.destroySpinner   
       */
      destroySpinner: function () {
        $('#loadingModal').hide();
        spinner.destroy();
      },
      
      /**
       * Create the 'loading' spinner.
       * @function  OrderViewModel.createSpinner
       */
      createSpinner: function (loadingText) {
        var indicatorOptions = {
          parent: '#loadingModal',
          posTop: '0',
          posLeft: '50%'
        };
         loadingText = CCi18n.t('ns.common:resources.loadingText');
        $('#loadingModal').removeClass('hide');
        $('#loadingModal').show();
        indicatorOptions.loadingText = loadingText;
        spinner.create(indicatorOptions);
      },
    
    onLoad : function(widget) {                      
    },
  
    beforeAppear : function(page) {
      var widget = this;
      widget.createSpinner();
      widget.ob_getnewscontent();  
    },

    /** Go Back to previous page **/
    ob_goBack: function(){      
      $(window).scrollTop(0);
        window.history.go(-1);
        return false;      
    },

    /** Get the News Details **/
    ob_getnewscontent: function(){        
      var widget=this;
      var url = widget.site().extensionSiteSettings.externalSiteSettings.OCECServerURL;
      var channelToken = widget.site().extensionSiteSettings.externalSiteSettings.channelToken;
      var languageCode = widget.site().extensionSiteSettings.externalSiteSettings.langCode;
      var params = window.location.search;	
      var contentItemID = (params.split('='))[1].toString();	
      $.ajax({   	
          url: url+'/content/published/api/v1.1/items?q=(id eq "'+ contentItemID +'" AND language eq "' + languageCode + '" )&channelToken=' + channelToken,
          type: 'GET',
          dataType: 'json',
          success: function(success) {
            
            if(success.items.length >0){
              var items = success.items[0];
                if(items.fields.news_image){
                  widget.ob_imageData(url+'/content/published/api/v1.1/assets/'+items.fields.news_image.id+'/Large/'+items.fields.news_image.name+'?format=jpg&type=responsiveimage&channelToken='+ channelToken);
                }else {
                  widget.ob_imageData('no-image.png');
                }
                widget.ob_newsdetails(items);
                widget.ob_newsTitle(items.fields.title[0]); 
                widget.destroySpinner();               
            }else{
              n.goTo('/home');
              widget.destroySpinner();
            }
              
             
          },
          error: function(error) {
              n.goTo('/home');
              widget.destroySpinner();    
           }
          });  
      }
  }   
}
);