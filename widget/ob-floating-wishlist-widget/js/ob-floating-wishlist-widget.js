/** Floating Wishlist Js **/
define(

  ['jquery','knockout', 'CCi18n','storageApi'],

  function($, ko, CCi18n , storageApi) {
    var wishlistcookie;
    
    "use strict";

    return {
      ob_showfloatingIcon: ko.observable(false),
      ob_numberofItemsadded: ko.observable(),
      resourcesLoaded : function(widget) {
      },

      onLoad : function(widget) { 

      },

      beforeAppear : function(page) {
         var widget = this;
         var wishlistcookie  = storageApi.getInstance().readFromCookies("obwishlistcookie");
         var cookie = JSON.parse(wishlistcookie);  
         
         /** show floating icon if items added to wishlist **/
         if(cookie){
          widget.ob_numberofItemsadded(cookie.length); 
          widget.ob_showfloatingIcon(true);
         }
         else{
               
          widget.ob_showfloatingIcon(false);
         }

         /** Add items from PDP **/
        $.Topic('addTowishlistItem.memory').subscribe(function(data,value){
            widget.ob_numberofItemsadded(value);
          if(data){
            widget.ob_showfloatingIcon(true);
          }  
          else{
            widget.ob_showfloatingIcon(false);  
          }         
        });

        /** Remove items from wishlist **/
        $.Topic('removeFromCookie.memory').subscribe(function(data,value){
            widget.ob_numberofItemsadded(value);
          if(data){
            widget.ob_showfloatingIcon(true);  
          }    
          else{
            widget.ob_showfloatingIcon(false);    
          }      
        });
      }      
    }
  }
);