/**
 * @fileoverview osborn wishlist Widget.
 *
 */
define(

	//-------------------------------------------------------------------
	// DEPENDENCIES
	//-------------------------------------------------------------------

	['jquery', 'knockout', 'CCi18n', 'storageApi', 'ccRestClient', 'spinner', 'navigation'],

	function ($, ko, CCi18n, storageApi, ccRestClient, spinner, n) {

		"use strict";

		return {
			ob_wishlistFormDetails: ko.observable({}),
			ob_wishlistRule: ko.observable({}),
			ob_wishlistProductData: ko.observable(''),
			ob_removewishlistItem: ko.observable(false),
			ob_removewishlistlastItem: ko.observable(true),
			ob_confirmremovewishlist: ko.observable(false),
			oB_showMessage: ko.observable(false),
			oB_hideMessage: ko.observable(false),
			oB_addWishlistMessage: ko.observable(false),
			oB_currentISDnumber : ko.observable(''),
			ob_currentdatatoremove: ko.observable(),
			
			/*
			 * Note that "this" is bound to the Widget View Model.
			 */
			resourcesLoaded: function (widget) {},

			/**
			 * Destroy the 'loading' spinner.
			 * @function  OrderViewModel.destroySpinner   
			 */
			destroySpinner: function () {
				$('#loadingModal').hide();
				spinner.destroy();
			},

			/**
			 * Create the 'loading' spinner.
			 * @function  OrderViewModel.createSpinner
			 */
			createSpinner: function (loadingText) {
				var indicatorOptions = {
					parent: '#loadingModal',
					posTop: '0',
					posLeft: '50%'
				};
				var loadingText = CCi18n.t('ns.common:resources.loadingText');
				$('#loadingModal').removeClass('hide');
				$('#loadingModal').show();
				indicatorOptions.loadingText = loadingText;
				spinner.create(indicatorOptions);
			},

			onLoad: function (widget) {

				widget.ob_wishlistFormDetails(new widget.ob_createWishlistFormDetails());
			//	widget.ob_removeItem();
	
			   
      /** fix for ie/edge/firefox to accept only numbers in input box **/
      ko.bindingHandlers.oB_numeric = {
			init: function (element, valueAccessor) {
				$(element).on("keydown", function (event) {
					// Allow: backspace, delete, tab, escape, and enter
					if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
						// Allow: Ctrl+A
						(event.keyCode == 65 && event.ctrlKey === true) ||
						// Allow: . ,
						(event.keyCode == 188 || event.keyCode == 190 || event.keyCode == 110) ||
						// Allow: home, end, left, right
						(event.keyCode >= 35 && event.keyCode <= 39)) {
						// let it happen, don't do anything
						return;
					}
					else {
						// Ensure that it is a number and stop the keypress
						if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
							event.preventDefault();
						}
					}
				});
			}
		}; 

				/** start validation rules for wishlist */

				ko.validation.registerExtenders();

				widget.ob_wishlistRule().isModified = function () {
					return (widget.ob_wishlistFormDetails().ob_wishlistFirstName().isModified() ||
						widget.ob_wishlistFormDetails().ob_wishlistLastName().isModified() ||
						widget.ob_wishlistFormDetails().ob_wishlistCompany().isModified() ||
						widget.ob_wishlistFormDetails().ob_wishlistCountry().isModified() ||
						widget.ob_wishlistFormDetails().ob_wishlistEmail().isModified() ||
						widget.ob_wishlistFormDetails().ob_wishlistPhone().isModified() ||
						widget.ob_wishlistFormDetails().ob_wishlistMessage().isModified()
					);
				};

				widget.ob_wishlistRule().isValid = function () {
					return (widget.ob_wishlistFormDetails().ob_wishlistFirstName.isValid() &&
						widget.ob_wishlistFormDetails().ob_wishlistLastName.isValid() &&
						widget.ob_wishlistFormDetails().ob_wishlistCompany.isValid() &&
						widget.ob_wishlistFormDetails().ob_wishlistCountry.isValid() &&
						widget.ob_wishlistFormDetails().ob_wishlistEmail.isValid() &&
						widget.ob_wishlistFormDetails().ob_wishlistPhone.isValid() &&
						widget.ob_wishlistFormDetails().ob_wishlistMessage.isValid()

					);
				};

				widget.ob_wishlistRule().validateNow = function () {
					widget.ob_wishlistFormDetails().ob_wishlistFirstName.isModified(true);
					widget.ob_wishlistFormDetails().ob_wishlistLastName.isModified(true);
					widget.ob_wishlistFormDetails().ob_wishlistCompany.isModified(true);
					widget.ob_wishlistFormDetails().ob_wishlistCountry.isModified(true);
					widget.ob_wishlistFormDetails().ob_wishlistEmail.isModified(true);
					widget.ob_wishlistFormDetails().ob_wishlistPhone.isModified(true);
					widget.ob_wishlistFormDetails().ob_wishlistMessage.isModified(true);

					return (widget.ob_wishlistRule().isValid());
				};

				widget.ob_wishlistRule().reset = function () {
					widget.ob_wishlistFormDetails().ob_wishlistFirstName('');
					widget.ob_wishlistFormDetails().ob_wishlistLastName('');
					widget.ob_wishlistFormDetails().ob_wishlistCompany('');
					widget.ob_wishlistFormDetails().ob_wishlistCountry('');
					widget.ob_wishlistFormDetails().ob_wishlistEmail('');
					widget.ob_wishlistFormDetails().ob_wishlistPhone('');
					widget.ob_wishlistFormDetails().ob_wishlistMessage('');
				};

				widget.ob_wishlistRule().resetModified = function () {
					widget.ob_wishlistFormDetails().ob_wishlistFirstName.isModified(false);
					widget.ob_wishlistFormDetails().ob_wishlistLastName.isModified(false);
					widget.ob_wishlistFormDetails().ob_wishlistCompany.isModified(false);
					widget.ob_wishlistFormDetails().ob_wishlistCountry.isModified(false);
					widget.ob_wishlistFormDetails().ob_wishlistEmail.isModified(false);
					widget.ob_wishlistFormDetails().ob_wishlistPhone.isModified(false);
					widget.ob_wishlistFormDetails().ob_wishlistMessage.isModified(false);
				};

				widget.ob_wishlistFormDetails().ob_wishlistFirstName.extend({
					required: {
						params: true,
						message: CCi18n.t('ns.ob-wishlist-widget:resources.errorFirstNameRequired')
					},
					minLength: {
						params: 2,
						message: CCi18n.t('ns.ob-wishlist-widget:resources.errorFirstNameMin', {
							length: 2
						})
					},
					maxLength: {
						params: 50,
						message: CCi18n.t('ns.ob-wishlist-widget:resources.errorFirstNameMax', {
							length: 50
						})
					},
					pattern: {
						params: '^[^0-9]*$', /** All Characters without numbers*/						
						message: CCi18n.t('ns.ob-wishlist-widget:resources.errorFirstNameCharacter')
					}
				});

				widget.ob_wishlistFormDetails().ob_wishlistLastName.extend({
					required: {
						params: true,
						message: CCi18n.t('ns.ob-wishlist-widget:resources.errorLastNameRequired')
					},
					minLength: {
						params: 2,
						message: CCi18n.t('ns.ob-wishlist-widget:resources.errorLastNameMin', {
							length: 2
						})
					},
					maxLength: {
						params: 50,
						message: CCi18n.t('ns.ob-wishlist-widget:resources.errorLastNameMax', {
							length: 50
						})
					},
					pattern: {						
						params: '^[^0-9]*$', /** All Characters without numbers*/												
						message: CCi18n.t('ns.ob-wishlist-widget:resources.errorLastNameCharacter')
					}
				});


				widget.ob_wishlistFormDetails().ob_wishlistCompany.extend({
					required: {
						params: true,
						message: CCi18n.t('ns.ob-wishlist-widget:resources.errorCompanyRequired')
					},
					minLength: {
						params: 2,
						message: CCi18n.t('ns.ob-wishlist-widget:resources.errorCompanyMin', {
							length: 2
						})
					},
					maxLength: {
						params: 50,
						message: CCi18n.t('ns.ob-wishlist-widget:resources.errorCompanyMax', {
							length: 50
						})
					}
				});

				widget.ob_wishlistFormDetails().ob_wishlistCountry.extend({
					required: {
						params: true,
						message: CCi18n.t('ns.ob-wishlist-widget:resources.errorCountryRequired')
					},
					minLength: {
						params: 2,
						message: CCi18n.t('ns.ob-wishlist-widget:resources.errorCountryMin', {
							length: 2
						})
					},
					maxLength: {
						params: 50,
						message: CCi18n.t('ns.ob-wishlist-widget:resources.errorCountryMax', {
							length: 50
						})
					}
				});

				widget.ob_wishlistFormDetails().ob_wishlistEmail.extend({
					required: {
						params: true,
						message: CCi18n.t('ns.ob-wishlist-widget:resources.errorEmailRequired')
					},
					minLength: {
						params: 8,
						message: CCi18n.t('ns.ob-wishlist-widget:resources.errorEmailMin', {
							length: 8
						})
					},
					maxLength: {
						params: 50,
						message: CCi18n.t('ns.ob-wishlist-widget:resources.errorEmailMax', {
							length: 50
						})
					},
					email: {
						params: true,
						message: CCi18n.t('ns.ob-wishlist-widget:resources.errorEmailCharacter')
					}
				});

				widget.ob_wishlistFormDetails().ob_wishlistPhone.extend({
					required: {
						params: true,
						message: CCi18n.t('ns.ob-wishlist-widget:resources.errorPhoneRequired')
					},
					minLength: {
						params: 5,
						message: CCi18n.t('ns.ob-wishlist-widget:resources.errorPhoneMin', {
							length: 5
						})
					},
					maxLength: {
						params: 15,
						message: CCi18n.t('ns.ob-wishlist-widget:resources.errorPhoneMax', {
							length: 15
						})
					},
					pattern: {
						params: '^[0-9]*$',
						/** numbers only */
						message: CCi18n.t('ns.ob-wishlist-widget:resources.errorPhoneCharacter')
					}
				});


				widget.ob_wishlistFormDetails().ob_wishlistMessage.extend({
					required: {
						params: true,
						message: CCi18n.t('ns.ob-wishlist-widget:resources.errorMessageRequired')
					},
					minLength: {
						params: 5,
						message: CCi18n.t('ns.ob-wishlist-widget:resources.errorMessageMin', {
							length: 5
						})
					},
					maxLength: {
						params: 250,
						message: CCi18n.t('ns.ob-wishlist-widget:resources.errorMessageMax', {
							length: 250
						})
					}
				});

				/** End validation rules for wishlist **/

			},

			/** construct wishlist form  details */
			ob_createWishlistFormDetails: function () {
				this.ob_wishlistFirstName = ko.observable('');
				this.ob_wishlistLastName = ko.observable('');
				this.ob_wishlistCompany = ko.observable('');
				this.ob_wishlistCountry = ko.observable('');
				this.ob_wishlistEmail = ko.observable('');
				this.ob_wishlistPhone = ko.observable('');
				this.ob_wishlistMessage = ko.observable('');
			},

			/** reset wishlist form  */
			ob_resetWishlistFormDetails: function () {
				this.ob_wishlistFormDetails().ob_wishlistFirstName('');
				this.ob_wishlistFormDetails().ob_wishlistLastName('');
				this.ob_wishlistFormDetails().ob_wishlistCompany('');
				this.ob_wishlistFormDetails().ob_wishlistCountry('');
				this.ob_wishlistFormDetails().ob_wishlistEmail('');
				this.ob_wishlistFormDetails().ob_wishlistPhone('');
				this.ob_wishlistFormDetails().ob_wishlistMessage('');
			},
			
			
			/** change event for select box  **/
            updateISDnumber: function(data, event) {
                var widget = this;
                var getValue = $(event.currentTarget).val();
                if(getValue){
                 widget.oB_currentISDnumber(getValue);
                };
            },

			/** to load wishlist data and remove item from wishlist **/
			ob_removeItem: function (currentdata) {
				var self = this,
					cookieData, wishlist = [];
					self.ob_currentdatatoremove(currentdata);
				var storeData = storageApi.getInstance().readFromCookies("obwishlistcookie");
				if (storeData) {
					cookieData = JSON.parse(storeData);
					self.ob_wishlistProductData(cookieData);
					self.oB_addWishlistMessage(false);
				} else {
					self.oB_addWishlistMessage(true);
				}
				if(self.ob_removewishlistlastItem() && currentdata && cookieData.length == 1){
				     $('#lastItem').modal('show');
				     self.ob_confirmremovewishlist(true);
				}
				if (currentdata && (cookieData.length > 0)) {
				    if(!self.ob_confirmremovewishlist()){
				        $('#removeItem').modal('show');
				    }
				    if(self.ob_removewishlistItem()){
					for (var j = 0; j < cookieData.length; j++) {
						if (currentdata.SKU['Item Number'] === cookieData[j].SKU['Item Number']){
							delete cookieData[j];
						} else {
							wishlist.push(cookieData[j]);
						}
					}
					if (wishlist.length > 0) {
						self.oB_addWishlistMessage(false);
						self.ob_wishlistProductData(wishlist);
						storageApi.getInstance().saveToCookies("obwishlistcookie", JSON.stringify(wishlist), 30);
						$.Topic('removeFromCookie.memory').publish(true, wishlist.length);
					} else if (wishlist.length === 0) {
						self.oB_addWishlistMessage(true);
						document.getElementById("wishlistSubmit").disabled = true;
						self.ob_wishlistProductData(wishlist);
						storageApi.getInstance().removeItem("obwishlistcookie");
						$.Topic('removeFromCookie.memory').publish(false, wishlist.length);
					}
				}
				}
				self.ob_removewishlistItem(false);
			},

            /** show pop up to remove item from wishlist **/
            ob_handleRemoveItem:function(){
                var widget = this;
                widget.ob_removewishlistItem(true);
                widget.ob_removeItem(widget.ob_currentdatatoremove());
            },
            
            ob_handlecancelRemove:function(){
                var widget = this;
                widget.ob_removewishlistItem(false);
                widget.ob_removeItem();
            },
            ob_lastItem: function(){
                var self = this;
                self.ob_confirmremovewishlist(false);
                self.ob_removewishlistlastItem(false);
            },

			/** submit contact form */
			ob_wishlistSendRequest: function () {
				var widget = this;
				widget.ob_wishlistRule().validateNow();
				if (!widget.ob_wishlistRule().isValid()) {
					return false;
				}

				if (widget.ob_wishlistRule().isValid()) {
					widget.createSpinner();
					var wishlistAPI = widget.site().extensionSiteSettings.externalSiteSettings.emailSSEurl ; /*** will change it to External site settings */
					var obWishlistData = {};
					obWishlistData.templateId = "wishlistEmail";
					obWishlistData.data = {};
        
					var obJSONdata = {};
					obJSONdata.obFirstName = widget.ob_wishlistFormDetails().ob_wishlistFirstName();
					obJSONdata.obLastName = widget.ob_wishlistFormDetails().ob_wishlistLastName();
					obJSONdata.obEmail = widget.ob_wishlistFormDetails().ob_wishlistEmail();
					obJSONdata.obPhone = widget.oB_currentISDnumber() + widget.ob_wishlistFormDetails().ob_wishlistPhone();
					obJSONdata.obCompany = widget.ob_wishlistFormDetails().ob_wishlistCompany();
					obJSONdata.obCountry = widget.ob_wishlistFormDetails().ob_wishlistCountry();
					obJSONdata.obMessage = widget.ob_wishlistFormDetails().ob_wishlistMessage();

					obJSONdata.obRegion = widget.site().extensionSiteSettings.externalSiteSettings.langCode;

					obJSONdata.obHost = location.protocol + '//' + location.hostname;
					obJSONdata.obToSystemEmail = $("#obToSystemEmail").val();

					obJSONdata.obProduct = [];
					for (var i = 0; i < widget.ob_wishlistProductData().length; i++) {
						var obProductItem = {};
						obProductItem.obProductImg = widget.ob_wishlistProductData()[i].productmediumImageURLs[0];
						obProductItem.obProductId = widget.ob_wishlistProductData()[i].productID;
						obProductItem.obProductName = widget.ob_wishlistProductData()[i].productname;
						obProductItem.obProductSku = widget.ob_wishlistProductData()[i].SKU.Ob_ItemNumber;
						obProductItem.obProductUrl = widget.ob_wishlistProductData()[i].productURL;
						obProductItem.obProductSkuDetails = widget.ob_wishlistProductData()[i].SKU;

						obJSONdata.obProduct.push(obProductItem);
					}

					obWishlistData.data = obJSONdata;

					ccRestClient.authenticatedRequest(wishlistAPI, obWishlistData, function (data) {
						//Show success Message
						if (data) {
							widget.destroySpinner();
							widget.oB_showMessage(true);
							widget.ob_wishlistProductData('');
							storageApi.getInstance().removeItem("obwishlistcookie");
							$.Topic('removeFromCookie.memory').publish(false, 0);
							if (!storageApi.getInstance().readFromCookies("obwishlistcookie")) {
								document.getElementById("wishlistSubmit").disabled = true;
							}
							$(window).scrollTop(0);
							$("#success-close").click(function(){
                                widget.oB_showMessage(false);
                            }); 
                            setTimeout(function() {
                             widget.oB_showMessage(false);
                            }, 60000);
						}
					}, function (err) {
						//Show error Message
						widget.destroySpinner();
						widget.oB_hideMessage(true);
						$(window).scrollTop(0);
						$("#failure-close").click(function(){
                           widget.oB_hideMessage(false);
                        });
                        setTimeout(function() {
                         widget.oB_hideMessage(false);
                        }, 60000);
					}, "POST");

					widget.ob_resetWishlistFormDetails();
					widget.ob_wishlistRule().resetModified(); 
				}

			},

			beforeAppear: function (page) {
				var widget = this;
				widget.ob_removeItem();
				//Initially Hide the Success and Error
				widget.oB_showMessage(false);
				widget.oB_hideMessage(false);

				if (!storageApi.getInstance().readFromCookies("obwishlistcookie")) {
					document.getElementById("wishlistSubmit").disabled = true;
				}

					 /** default value for select box corresponding to site**/
					 switch(widget.locale()){
						case "en":
							   $('select option:contains("United States (+1)")').prop('selected',true);
							   break;
						case "en_US":
							   $('select option:contains("United States (+1)")').prop('selected',true);
							   break; 
						case "de":
							$('select option:contains("Germany (+49)")').prop('selected',true);
							break;   
						case "es":
							$('select option:contains("Spain (+34)")').prop('selected',true);
							break;      
						case "fr":
							$('select option:contains("France (+33)")').prop('selected',true);
							break;  
						case "sv":
							$('select option:contains("Sweden (+46)")').prop('selected',true);
							break;      
					}
			},

			//Navigate Product Details
			obNavigatePDP : function(data){
				if(data.availableInBothCatalog === true){
				    n.goTo(data.productURL);
				} else {
					//n.goTo();
					var currentSite = this.locale();
					if((currentSite === "en_US" && data.catalog === "EU") || (currentSite === "es" && data.catalog === "EU")){
						var siteRedirect = JSON.parse(storageApi.getInstance().readFromCookies("obsitecookie"));
				    	siteRedirect.userselected = data.origin;
				    	storageApi.getInstance().saveToCookies("obsitecookie", JSON.stringify(siteRedirect), 30);
				    	document.location.href = window.location.origin +'/'+data.origin+data.productURL;
					} else {
						n.goTo(data.productURL);
					}
				    
				}
				
			}
		};
	}
);