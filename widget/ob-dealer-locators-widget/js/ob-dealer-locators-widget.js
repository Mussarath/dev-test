define(

	['jquery', 'knockout', 'CCi18n', 'ccRestClient', 'spinner', 'navigation'],

	function ($, ko, CCi18n, ccRestClient, spinner, navigation) {

		"use strict";
		var distributerDetails = '';
		var apiLatitude, apiLongitude, dealerGrid = [], ob_refineSelectedFilters = [], ob_dataForFilter;

		return {
			ob_currentPage: ko.observable(''),
			ob_pageNumber: ko.observable(''),
			ob_totalNumberOfPages: ko.observable(''),
			ob_dealerLocatorFormDetails: ko.observable({}),
			ob_dealerLocatorRule: ko.observable({}),  
			ob_totalDistributorRecords: ko.observableArray([]),  
			ob_dealerData: ko.observableArray([]),
			ob_distributorData: ko.observable(),
			noRecordFound: ko.observable(false),  
			ob_pages: ko.observableArray([]),
			ob_getClassName: ko.observable(''),
			/*
			 * Note that "this" is bound to the Widget View Model.     
			 */
			resourcesLoaded: function (widget) {},
			/**
			 * Destroy the 'loading' spinner.
			 * @function  OrderViewModel.destroySpinner   
			 */
			destroySpinner: function () {
				$('#loadingModal').hide();
				spinner.destroy();
			},

			/**
			 * Create the 'loading' spinner.
			 * @function  OrderViewModel.createSpinner
			 */
			createSpinner: function (loadingText) {
				var indicatorOptions = {
					parent: '#loadingModal',
					posTop: '0',
					posLeft: '50%'
				};
				var loadingText = CCi18n.t('ns.common:resources.loadingText');
				$('#loadingModal').removeClass('hide');
				$('#loadingModal').show();
				indicatorOptions.loadingText = loadingText;
				spinner.create(indicatorOptions);
			},
			onLoad: function (widget) {
				widget.ob_dealerLocatorFormDetails(new widget.ob_createDealerLocatorFormDetails());


				/** start validation rules for DealerLocator */  

				ko.validation.registerExtenders();

				widget.ob_dealerLocatorRule().isModified = function () {
					return (widget.ob_dealerLocatorFormDetails().ob_dealerLocatorCity().isModified() ||
						widget.ob_dealerLocatorFormDetails().ob_dealerLocatorCountry().isModified() ||
						widget.ob_dealerLocatorFormDetails().ob_dealerLocatorDistance().isModified()

					);
				};

				widget.ob_dealerLocatorRule().isValid = function () {
					return (widget.ob_dealerLocatorFormDetails().ob_dealerLocatorCity.isValid() &&
						widget.ob_dealerLocatorFormDetails().ob_dealerLocatorCountry.isValid() &&
						widget.ob_dealerLocatorFormDetails().ob_dealerLocatorDistance.isValid()

					);
				};

				widget.ob_dealerLocatorRule().validateNow = function () {

					widget.ob_dealerLocatorFormDetails().ob_dealerLocatorCity.isModified(true);
					widget.ob_dealerLocatorFormDetails().ob_dealerLocatorCountry.isModified(true);
					widget.ob_dealerLocatorFormDetails().ob_dealerLocatorDistance.isModified(true);


					return (widget.ob_dealerLocatorRule().isValid());
				};

				widget.ob_dealerLocatorRule().reset = function () {

					widget.ob_dealerLocatorFormDetails().ob_dealerLocatorCity('');
					widget.ob_dealerLocatorFormDetails().ob_dealerLocatorCountry('');
					widget.ob_dealerLocatorFormDetails().ob_dealerLocatorDistance('');

				};

				widget.ob_dealerLocatorRule().resetModified = function () {

					widget.ob_dealerLocatorFormDetails().ob_dealerLocatorCity.isModified(false);
					widget.ob_dealerLocatorFormDetails().ob_dealerLocatorCountry.isModified(false);
					widget.ob_dealerLocatorFormDetails().ob_dealerLocatorDistance.isModified(false);
				};

				widget.ob_dealerLocatorFormDetails().ob_dealerLocatorCity.extend({
					required: {
						params: true,
						message: CCi18n.t('ns.ob-dealer-locators-widget:resources.errorCityRequired')
					},
					minLength: {
						params: 2,
						message: CCi18n.t('ns.ob-dealer-locators-widget:resources.errorCityMin', {
							length: 2
						})
					},
					maxLength: {
						params: 50,
						message: CCi18n.t('ns.ob-dealer-locators-widget:resources.errorCityMax', {
							length: 50
						})
					},
					pattern: {
						params: '^[ a-zA-Z0-9][a-zA-Z0-9\\s]*$',
						/** alphabets, numbers and space */
						message: CCi18n.t('ns.ob-dealer-locators-widget:resources.errorCityCharacter')
					}
				});

				widget.ob_dealerLocatorFormDetails().ob_dealerLocatorCountry.extend({
				    
					required: {
						params: true,
						message: CCi18n.t('ns.ob-dealer-locators-widget:resources.errorCountryRequired')
					},
					minLength: {
						params: 2,
						message: CCi18n.t('ns.ob-dealer-locators-widget:resources.errorCountryMin', {
							length: 2
						})
					},
					maxLength: {
						params: 50,
						message: CCi18n.t('ns.ob-dealer-locators-widget:resources.errorCountryMax', {
							length: 50
						})
					},
					pattern: {
						params: '^[ a-zA-Z][a-zA-Z\\s]*$',
						/** alphabets and space */
						message: CCi18n.t('ns.ob-dealer-locators-widget:resources.errorCountryCharacter')
					}
				});

				widget.ob_dealerLocatorFormDetails().ob_dealerLocatorDistance.extend({
					required: {
						params: true,
						message: CCi18n.t('ns.ob-dealer-locators-widget:resources.errorDistanceRequired')
					},
					minLength: {
						params: 2,
						message: CCi18n.t('ns.ob-dealer-locators-widget:resources.errorDistanceMin', {
							length: 2
						})
					},
					maxLength: {
						params: 10,
						message: CCi18n.t('ns.ob-dealer-locators-widget:resources.errorDistanceMax', {
							length: 10
						})
					},
					pattern: {
						params: '^[0-9]*$',
						/** numbers only */
						message: CCi18n.t('ns.ob-dealer-locators-widget:resources.errorDistanceCharacter')
					}
				});


				/** End validation rules for DealerLocator */

				//Get Distributor Details
				if (window.hasOwnProperty('distributorAddresses') && window.distributorAddresses) {
					distributerDetails = window.distributorAddresses;
				} else {
					//Get Distributor Details From OCC Files
					widget.getDistributorAddresses();
				}

				//Refine Checkbox Change
				$('body').on('change', "input[type=checkbox]", function (e) {
                    if (widget.ob_totalDistributorRecords().length > 0 ) {
                    if(ob_dataForFilter){
                        if($(this).prop('checked')){
                            ob_refineSelectedFilters.push($(this).val());
                         }else {
                             var i = ob_refineSelectedFilters.length, x, arr = ob_refineSelectedFilters;
                             for(x=0; x<i; x++){
                                 if(arr[x] === $(this).val()){
                                     ob_refineSelectedFilters.splice(x, 1);
                                 }
                             }
                         }
                         widget.ob_checkData();
                    } else {
						widget.getDistributor();
                    }
                }
				});

			},

			ob_checkData: function () {
			    var widget = this;
				if (widget.ob_totalDistributorRecords()) {
					//Perform Refine Search
					widget.ob_refineSearch();
				}
			},

			//Perform Refine Search
			ob_refineSearch: function () {
                var widget = this, x = ob_dataForFilter.length, 
                data = ob_dataForFilter,
                swapData = [], filterLength = ob_refineSelectedFilters.length;
                if(filterLength > 0){
                    while (x--) {
                        for(var j=0; j<filterLength; j++){
                            if (data[x]['Products Carried'].indexOf(ob_refineSelectedFilters[j]) > -1 || data[x]['Distributor Type'].indexOf(ob_refineSelectedFilters[j]) > -1) {
								swapData.push(data[x]);
								break;
                            }
                        }   
                        
                    }
                } else {
                    swapData = data;
                }
				
				widget.ob_totalDistributorRecords(swapData);
                widget.ob_paginationRecords();
			},

			//Get The Distributor JSON File and store in browser for performance
			getDistributorAddresses: function () {
				//Based on Site US or EU Construct the URL 
                //File Read
                if(window.hasOwnProperty('distributorAddresses') && window.distributorAddresses.length > 0){
                    distributerDetails = window.distributorAddresses;
                } else {
                    ccRestClient.authenticatedRequest("/ccstoreui/v1/files/thirdparty/DistributorLocator.json", {}, function (data) {
                        if (data) {
                            window.distributorAddresses = data;
                            distributerDetails = data;
                        }
                    }, function (err) {}, "GET");
                }
				
			},

			/** construct DealerLocator form  details */
			ob_createDealerLocatorFormDetails: function () {
                var widget = this;
				widget.ob_dealerLocatorCity = ko.observable('');
				widget.ob_dealerLocatorCountry = ko.observable('US');
				widget.ob_dealerLocatorDistance = ko.observable('100');

			},

			/** reset DealerLocator form  */
			ob_resetDealerLocatorFormDetails: function () {
                var widget = this;
				widget.ob_dealerLocatorCity = ko.observable('');
				widget.ob_dealerLocatorCountry = ko.observable('US');
				widget.ob_dealerLocatorDistance = ko.observable('100');
			},

			/** submit DealerLocator form */
			ob_dealerLocatorSendRequest: function () {
				var widget = this;

				widget.ob_dealerLocatorRule().validateNow();

				if (!widget.ob_dealerLocatorRule().isValid()) {
					return false;
				}
				//Create Spinner
				widget.createSpinner();
				//Get Location Method 
				widget.getLocation();

			},
			//Get Location using TomTom API Key
			getLocation: function () {
				var widget = this,
					radius = parseInt(widget.ob_dealerLocatorFormDetails().ob_dealerLocatorDistance()) * 1000,
					zipCity = widget.ob_dealerLocatorFormDetails().ob_dealerLocatorCity(),
					country = widget.ob_dealerLocatorFormDetails().ob_dealerLocatorCountry(),
					url = 'https://api.tomtom.com/search/2/search/',
					key = widget.site().extensionSiteSettings.externalSiteSettings.tomTomKey;
					//key = '38wBAOfKrPLI7GGcEVRzxlY2wYiYcnBW';
					//Reset No Search Result found
					widget.noRecordFound(false);
				//Ajax Call for API Get Method
				$.ajax({
					type: "GET",
					url: url + zipCity + ".json?key=" + key + "&countrySet=" + country + "&limit=1",
					success: function (data, status) {
						if (status && status === 'success') {
							if (data.summary && data.summary.numResults > 0) {
								apiLatitude = data.results["0"].position.lat;
								apiLongitude = data.results["0"].position.lon;
								widget.getDistributor(radius);
							} else {
								//Failure Case
								widget.noRecordFound(true);
								widget.ob_totalDistributorRecords([]);
								widget.ob_distributorData([]);
								ob_dataForFilter = [];
								widget.ob_paginationRecords();
								widget.destroySpinner();
							}
						} else {
							//Failure Case
							widget.noRecordFound(true);
							widget.ob_totalDistributorRecords([]);
							widget.ob_distributorData([]);
							ob_dataForFilter = [];
							widget.ob_paginationRecords();
							widget.destroySpinner();
						}
					},
					error: function (err) {

						widget.noRecordFound(true);
						widget.ob_totalDistributorRecords([]);
						widget.ob_distributorData([]);
						ob_dataForFilter = [];
						widget.ob_paginationRecords();
						widget.destroySpinner();
					}
				});

			},

			//Get Distributor Details for Searched Data
			getDistributor: function () {
				var widget = this;

				if (!distributerDetails) {
					widget.getDistributorAddresses();
				}
				widget.ob_totalDistributorRecords([]);
				if (distributerDetails) {
                    var i = distributerDetails.length;
                    //Check for Refined Search
					var checkedValues = [];
					$.each($("input[type='checkbox']:checked"), function () {
						checkedValues.push($(this).val());
                    });
                    var checkedLength = checkedValues.length;
					while (i--) {
						var distance = widget.getDistanceFromLatLonInKm(apiLatitude, apiLongitude, distributerDetails[i].Lat, distributerDetails[i].Lng);
						if (parseInt(widget.ob_dealerLocatorFormDetails().ob_dealerLocatorDistance()) && distance > parseInt(widget.ob_dealerLocatorFormDetails().ob_dealerLocatorDistance())) {
							continue;
						}
						if (distributerDetails[i].hasOwnProperty('Parent of Customer') && distributerDetails[i]['Parent of Customer']) {
                            if(checkedLength > 0){
                                for(var z=0; z<checkedLength; z++){
                                    if(distributerDetails[i]['Products Carried'] === checkedValues[z] || distributerDetails[i]['Distributor Type'] === checkedValues[z]){
										widget.ob_totalDistributorRecords.push(distributerDetails[i]);
										break;
                                    }
                                }
                            } else {
								if(widget.ob_dealerLocatorFormDetails().ob_dealerLocatorCountry() === distributerDetails[i].Country){
								widget.ob_totalDistributorRecords.push(distributerDetails[i]);
								}
                            }
						} 
					}
                }
                ob_dataForFilter = widget.ob_totalDistributorRecords();

				widget.ob_resetDealerLocatorFormDetails();
				widget.destroySpinner();
				widget.oB_resizeLocalDistriHeight();
				var $target = $('html,body');
				$target.animate({
					scrollTop: $target.height()
				}, 1000);
				
				widget.ob_paginationRecords();

				// list view functionality  
				$("body").delegate("#list", "click", function () {
					$('#products .item').addClass('list-group-item');
					$("#products .item").removeClass("grid-group-item");
					$(".AddressWidth label").removeClass("col-sm-3");
					$(".AddressWidth label").addClass("col-sm-1");
					$(".displayBlock").removeClass("col-sm-12 col-xs-12");
					$(".displayBlock label").removeClass("col-sm-3 col-xs-3");
					$(".displayBlock a").removeClass("col-sm-9 col-xs-9");
					$(".displayBlock span").removeClass("col-sm-9 col-xs-9");
					$(".padLeft:odd").removeClass("gridoddPadLeft");
					$(".padRight:even").removeClass("gridevenPadRight");
					$(".padLeft:even").removeClass("gridevenPadLeft");
					$(".padRight:odd").removeClass("gridoddPadRight");
					$("#list").addClass("activegrid");
					$("#grid").removeClass("activegrid");
					widget.ob_getClassName($('#products .item').attr('class'));
					widget.oB_resizeLocalDistriHeight();
				});
				// grid view functionality
				$("body").delegate("#grid", "click", function () {
					$('#products .item').removeClass('list-group-item');
					$('#products .item').addClass('grid-group-item');
					$(".AddressWidth label").removeClass("col-sm-1");
					$(".AddressWidth label").addClass("col-sm-3");
					$(".displayBlock").addClass("col-sm-12 col-xs-12");
					$(".displayBlock label").addClass("col-sm-3");
					$(".padLeft:odd").addClass("gridoddPadLeft");
					$(".padRight:even").addClass("gridevenPadRight");
					$(".padLeft:even").addClass("gridevenPadLeft");
					$(".padRight:odd").addClass("gridoddPadRight");
					$("#grid").addClass("activegrid");
					$("#list").removeClass("activegrid");
					widget.ob_getClassName($('#products .item').attr('class'));
					widget.oB_resizeLocalDistriHeight();
				});
				$('#grid').trigger('click');
				// checks the container is visible on the page 
				function checkContainer() {
					if ($('#products .item').is(':visible')) {
						var evt = window.document.createEvent('UIEvents');
						evt.initUIEvent('resize', true, false, window, 0);
						window.dispatchEvent(evt);
						widget.oB_resizeLocalDistriHeight();
					} else {
						setTimeout(checkContainer, 50); //wait 50 ms, then try again
					}
				}
				// function available after the document is loaded. 
				$(document).ready(checkContainer);

			},

            //trigger list view, grid view
            ob_triggerview: function(){
                var widget = this;
                if (widget.ob_getClassName() && widget.ob_getClassName() == "list-group-item" || (widget.ob_getClassName().indexOf("list-group-item") !== -1)) {
					$('#list').trigger('click');
				} else if (widget.ob_getClassName() && widget.ob_getClassName() == "grid-group-item" || (widget.ob_getClassName().indexOf("grid-group-item")!== -1)) {
					$('#grid').trigger('click');
				}
				var $target = $('html,body');
				$target.animate({
					scrollTop: $target.height()
				}, 1000);
            },
            
			//Pagination based on records  
			ob_paginationRecords: function(){
			    var widget = this;
			    widget.ob_pages([]);
				var totalRecords = widget.ob_totalDistributorRecords().length;
				var paginated_Numbers = (Math.round(totalRecords / 6) > 0) ? Math.round(totalRecords / 6) : 0;
				widget.ob_totalNumberOfPages(paginated_Numbers);
				if(paginated_Numbers !== 1){
				for (var j = 0; j < paginated_Numbers; j++) {
					widget.ob_pages.push({
						"pageNumber": j + 1
					})
				}
			}
				var startIndex = 0;
				var endIndex = (widget.ob_totalDistributorRecords().length > 5) ? 5 : widget.ob_totalDistributorRecords().length;
				if (paginated_Numbers > 1) {
					dealerGrid = [];
					while (paginated_Numbers--) {
						widget.ob_dealerData([]);

						for (var k = startIndex; k <= endIndex; k++) {
							if (widget.ob_totalDistributorRecords()[k]) {
								widget.ob_dealerData.push(widget.ob_totalDistributorRecords()[k]);
							}
						}
						dealerGrid.push(widget.ob_dealerData())
						startIndex += 6;
						endIndex += 6;
					}
				} else {
					widget.ob_dealerData([]);
					dealerGrid = [];
					for (var k = startIndex; k <= endIndex; k++) {
						if (widget.ob_totalDistributorRecords()[k]) {
							widget.ob_dealerData.push(widget.ob_totalDistributorRecords()[k]);
						}
					}
					dealerGrid.push(widget.ob_dealerData());
				}
				widget.ob_selectedPage(0);
			},
			
			ob_selectedPage: function (index) {
				var widget = this;
				widget.ob_distributorData(dealerGrid[index]);
				widget.ob_currentPage(index + 1);
				widget.ob_pageNumber(index);
				widget.ob_triggerview();
			},
			ob_firstPage: function () {
				var widget = this;
				widget.ob_distributorData(dealerGrid[0]);
				widget.ob_currentPage(1);
				widget.ob_pageNumber(0);
				widget.ob_triggerview();
			},
			ob_previousPage: function () {
				var widget = this;
				if ((widget.ob_pageNumber() - 1) > 0) {
					widget.ob_distributorData(dealerGrid[widget.ob_pageNumber() - 1]);
					widget.ob_pageNumber(widget.ob_pageNumber() - 1);
					widget.ob_currentPage(widget.ob_currentPage() - 1)
				} else {
					widget.ob_pageNumber(0);
					widget.ob_currentPage(1);
					widget.ob_distributorData(dealerGrid[0]);
				}
				widget.ob_triggerview();
			},
			ob_nextPage: function () {
				var widget = this;
				widget.ob_distributorData(dealerGrid[widget.ob_pageNumber() + 1]);
				widget.ob_pageNumber(widget.ob_pageNumber() + 1);
				widget.ob_currentPage(widget.ob_currentPage() + 1);
				widget.ob_triggerview();
			},
			ob_lastPage: function () {
				var widget = this;
				var lastPage = Math.round((widget.ob_totalDistributorRecords().length) / 6)
				widget.ob_distributorData(dealerGrid[lastPage - 1]);
				widget.ob_pageNumber(lastPage - 1);
				widget.ob_currentPage(lastPage);
				widget.ob_triggerview();
			},
			//ends here

			//Get the Distance in KM based on Latitude and Longitude
			getDistanceFromLatLonInKm: function (lat1, lon1, lat2, lon2) {
			    var widget = this;
				var R = 3959; // Radius of the earth in miles
				var dLat = widget.deg2rad(lat2 - lat1); // deg2rad below
				var dLon = widget.deg2rad(lon2 - lon1);
				var a =
					Math.sin(dLat / 2) * Math.sin(dLat / 2) +
					Math.cos(widget.deg2rad(lat1)) * Math.cos(widget.deg2rad(lat2)) *
					Math.sin(dLon / 2) * Math.sin(dLon / 2);
				var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
				var d = R * c; // Distance in miles
				return d.toFixed(1) * 1;
			},

			deg2rad: function (deg) {
				return deg * (Math.PI / 180);
			},

			// Local Distributor item Details height resize functionality  
			oB_resizeLocalDistriHeight: function () {
			    
				var highestBox = 0;

				if ($("#products .item").hasClass("grid-group-item")) {
					$('#products .item').each(function () {
						if ($(this).height() > highestBox) {
							highestBox = $(this).height();
						}
					});
					$('#products .item').height(highestBox);
				} else {
					$(".list-group-item").css("height", "unset");
				}
			},
			// ends here

			//   redirection to contact page functionality
			redirectTOContact: function (data) {
				navigation.goTo("/osbornContactUs?CompanyName=" + data.Customer + "&&Address=" + data.Street + "," + data.City + "," + data.Country + "," + data.PostalCode);
			},
			// ends here  
            
            ob_resetsearch: function(){
                var widget = this;
              	widget.ob_currentPage('');
				widget.ob_pageNumber('');
				widget.ob_totalNumberOfPages('');
				widget.ob_totalDistributorRecords([]);
				widget.ob_dealerData([]);
				widget.ob_distributorData('');
				widget.ob_pages([]);
     			widget.ob_dealerLocatorFormDetails().ob_dealerLocatorCity(''),
     			widget.ob_dealerLocatorFormDetails().ob_dealerLocatorCity.isModified(false);  
            },
            
			beforeAppear: function (page) {
				var widget = this;
				widget.ob_resetsearch();
				//Reset No Search Result found
				widget.noRecordFound(false);
				// On window resizing this function will work 
				$(window).resize(function () {
					widget.oB_resizeLocalDistriHeight();
				});
			}

		}
	}
);