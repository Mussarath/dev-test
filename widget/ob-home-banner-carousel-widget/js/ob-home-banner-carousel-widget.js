/**
 * @fileoverview Home banner carousel.
 */

define(

	['knockout', 'CCi18n' , 'js/jquery.touchSwipe'],
  
	function (ko, CCi18n) {
  
	  "use strict";
  
	  return {
		ob_mobilescreen : ko.observable(false),
		ob_tabletscreen : ko.observable(false),
        ob_isScreenWidthSet : ko.observable(false),
		/*
		 * Note that "this" is bound to the Widget View Model.
		 */
		resourcesLoaded: function (widget) {},
  
		/** start find device type **/
		findDeviceType: function () {
		  var widget = this, screenWidth = window.innerWidth;
		  if ((screenWidth >= 320) && (screenWidth <= 767)) {
			widget.ob_mobilescreen(true);
			widget.ob_tabletscreen(false);
		  }else if ((screenWidth >= 768) && (screenWidth <= 991)){
				widget.ob_tabletscreen(true);
				widget.ob_mobilescreen(false);
		  }else if ((screenWidth >= 992 )){
			widget.ob_tabletscreen(false);
			widget.ob_mobilescreen(false);
	  }
		  widget.ob_isScreenWidthSet(true);
		  /** Mobile swipe functionality **/
		  $(".carousel").swipe({
			swipe: function(event, direction, distance, duration, fingerCount, fingerData) {
			  if (direction == 'left') $(this).carousel('next');
			  if (direction == 'right') $(this).carousel('prev');
			},
			allowPageScroll:"vertical"
		  });
		  /** Ends here **/
		  
		},
		/** End find device type **/
        
        
        
		onLoad: function (widget) {
		  /** start resize function for screen **/
		  $(window).resize(function () {
			widget.findDeviceType();
			/** End resize function for screen **/
		  });
		},
  
		beforeAppear: function (page) {		  
		  var widget = this;  
		  widget.findDeviceType();		
		}
	  };
	}
  );