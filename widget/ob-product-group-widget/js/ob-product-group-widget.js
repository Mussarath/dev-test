/** fileoverview collection left navigation  **/
define(

  ['knockout', 'CCi18n', 'ccRestClient'],

  function(ko, CCi18n ,ccRestClient ) {

    "use strict";

    return {
      ob_collections: ko.observable(),
      resourcesLoaded : function(widget) {
      },

      onLoad : function(widget) { 
      },
      
      //Get the Categories list
      ob_getCategory: function(widget){
          var widget = this;
          var getCategoryPath = widget.category().categoryIdPaths[0];
          if(getCategoryPath.split('>').length>1){
              var getColletionID = getCategoryPath.split('>')[2];
          }
            var l = {};
            l['expand'] = 'childCategories';
            l['fields'] = 'childCategories,displayName,longDescription';
            l['maxLevel'] = 1000;
            ccRestClient.authenticatedRequest("/ccstoreui/v1/collections/"+getColletionID,l, function (data) {
              if(data){
                widget.ob_collections(data);
                }
                }, function (err) {
            }, "GET");
      },
      
       beforeAppear : function(page) {
          var widget = this;
          //Get Categories call
          widget.ob_getCategory();
      },          
    }
  }
);