define(

    ['knockout', 'CCi18n', 'pubsub', 'viewModels/guidedNavigationViewModel', 'spinner', 'storageApi', 'navigation'],
  
    function(ko, CCi18n, pubsub, GuidedNavigationViewModel, spinner, storageApi, nav) {
  
        "use strict";
        var getWidget = '';
        var ajaxRequests = [];
        var languageCode = '';
        var channelToken = '';
        var OCECURL = '';
        var responsecounter = 0;
        var queryconstructedArray = [];
        var isBackfromNewsLanding = false;
        return {
            maxDimensionCount: ko.observable("10"),
            maxRefinementCount: ko.observable("10"),
            obSearchTerm: ko.observable(''),
            isNewsCategory: ko.observable(false),
            obContentItems: ko.observableArray([]),
            obNoNewsFound: ko.observable(''),
            obpagination: ko.observableArray([]),
            obcurrentpage: ko.observable(1),
            obtotalNumberOfPages: ko.observable(1),
            obSelectedNewsfilters: ko.observableArray([]),
            getcurrentpage: ko.observable(''),
            /*
             * Note that "this" is bound to the Widget View Model.
             */
            resourcesLoaded: function(widget) {},
            /**
             * Destroy the 'loading' spinner.
             * @function  OrderViewModel.destroySpinner
             */
            destroySpinner: function() {
                $('#loadingModal').hide();
                spinner.destroy();
            },
            /** Method used to Redirect for News Listing to News landing page */
            obnewsredirection: function(data, event) {
                $(window).scrollTop(0);
                var obj = {};
                obj.searchText = getWidget.obSearchTerm();
                obj.filtervalues = ko.toJS(getWidget.obSelectedNewsfilters());
                obj.currentpage = getWidget.getcurrentpage();
                storageApi.getInstance().saveToCookies("obnewsfiltersCookie", JSON.stringify(obj), 1);
                nav.goTo('/obNewsLandingPage?Id=' + data);
            },
  
            /**
             * Create the 'loading' spinner.
             * @function  OrderViewModel.createSpinner
             */
            createSpinner: function(loadingText) {
                var indicatorOptions = {
                    parent: '#loadingModal',
                    posTop: '0',
                    posLeft: '50%'
                };
                var loadingText = CCi18n.t('ns.common:resources.loadingText');
                $('#loadingModal').removeClass('hide');
                $('#loadingModal').show();
                indicatorOptions.loadingText = loadingText;
                spinner.create(indicatorOptions);
            },
            /**Method used for Construct Search Params for selected filters */
            constructsearchparamsforFliters: function(filtervalues) {
                var widget = getWidget;
                var filtervaluesLength = filtervalues.length;
                var x = 0;
                queryconstructedArray = [];
  
                while (x < filtervaluesLength) {
                  if(typeof(filtervalues[x].selectedValue) !=="function"){
                      filtervalues[x].selectedValue = ko.observableArray(filtervalues[x].selectedValue);
                  }
                    if (filtervalues[x].displayName == "News type") {
                        
                        var Newstype = [];
                        for (var l = 0; l < filtervalues[x].selectedValue().length; l++) {
                            if (filtervalues[x].selectedValue().length > 0) {
                                Newstype.push("fields.news_type eq " + JSON.stringify(filtervalues[x].selectedValue()[l]));
                            }
                        }
                        queryconstructedArray.push('( ' + Newstype.join(' OR ') + ' )');
                    } else if (filtervalues[x].displayName == "Product Category") {
                        var productCategory = [];
                        for (var l = 0; l < filtervalues[x].selectedValue().length; l++) {
                            if (filtervalues[x].selectedValue().length > 0) {
                                productCategory.push("fields.product_category eq " + JSON.stringify(filtervalues[x].selectedValue()[l]));
                            }
                        }
                        queryconstructedArray.push('( ' + productCategory.join(' OR ') + ' )');
                    } else if (filtervalues[x].displayName == "Month") {
                        var fliteredMonth = [];
                        for (var l = 0; l < filtervalues[x].selectedValue().length; l++) {
                            if (filtervalues[x].selectedValue().length > 0) {
                                fliteredMonth.push("fields.month eq " + JSON.stringify(filtervalues[x].selectedValue()[l]));
                            }
                        }
                        queryconstructedArray.push('( ' + fliteredMonth.join(' OR ') + ' )');
                    } else if (filtervalues[x].displayName == "year") {
                        var fliteredyear = [];
                        for (var l = 0; l < filtervalues[x].selectedValue().length; l++) {
                            if (filtervalues[x].selectedValue().length > 0) {
                                fliteredyear.push("fields.year eq " + JSON.stringify(filtervalues[x].selectedValue()[l]));
                            }
                        }
                        queryconstructedArray.push('( ' + fliteredyear.join(' OR ') + ')');
                    }
                    x++;
                }
                widget.obContentItems([])
                widget.fetchNewsDetails(0);
  
            },
  
            onLoad: function(widget) {
                getWidget = widget;
                queryconstructedArray = [];
                $.Topic('obclearAllRefinements').subscribe(function(v){
                  queryconstructedArray =[]; 
                  widget.obContentItems([]);
                  widget.obSelectedNewsfilters([]);
                  widget.fetchNewsDetails(0);
                 
                });
                /** Method used to Swap to News from Products */
                $.Topic("swapToNews").subscribe(function() {
                    widget.obNoNewsFound('');
                    if ((document.cookie.indexOf('obnewsfiltersCookie') == -1)) {
                        /* inital offset value is zero */
                        widget.obContentItems([]);
                        widget.fetchNewsDetails(0);
                        getWidget.isNewsCategory(true);
                    } else {
                        var obnewsfiltersCookie = JSON.parse(storageApi.getInstance().readFromCookies("obnewsfiltersCookie"));
                        if (obnewsfiltersCookie.currentpage != widget.getcurrentpage()) {
                            /* inital offset value is zero */
                            widget.obContentItems([]);
                            widget.fetchNewsDetails(0);
                            getWidget.isNewsCategory(true);
                        }
                    }
  
  
                });
                
                /** Method used to Swap to Products  from News */
                $.Topic("swapToProducts").subscribe(function() {
                    getWidget.isNewsCategory(false);
                });
                /* pub sub is used get the selected filters from ProductNews navigation */
                $.Topic("obSelectedfliters").subscribe(function(flitervalues) {
                    widget.obSelectedNewsfilters(flitervalues);
                    widget.constructsearchparamsforFliters(flitervalues);
                });
                widget.guidedNavigationViewModel = ko.observable();
                widget.guidedNavigationViewModel(new GuidedNavigationViewModel(widget.maxDimensionCount(), widget.maxRefinementCount(), widget.locale()));
            },
            /** Method used to Fetch items from OCEC  */
            fetchNewsDetails: function(offset) {
                var widget = getWidget;
                if (widget.guidedNavigationViewModel().searchText()) {
                    if(widget.getcurrentpage() != "news"){
                        widget.obSearchTerm(widget.guidedNavigationViewModel().searchText());
                    } 
                }
                if (widget.obContentItems().length == 0) {
                    widget.createSpinner();
                    var newsurl = '';
                    if (widget.obSearchTerm()) {
                        if (queryconstructedArray.length > 0) {
                            var queryparams = queryconstructedArray.join('AND');
                            newsurl = OCECURL + '/content/published/api/v1.1/items?limit=' + widget.numberofNewsperpage() + '&offset=' + offset + '&q=(type eq ' + JSON.stringify(widget.obcontenttype()) + ' AND language eq ' + JSON.stringify(languageCode) + ' AND fields.news_content co "' + widget.obSearchTerm() + '" AND ' + queryparams + ')&channelToken=' + channelToken;
                        } else {
                            newsurl = OCECURL + '/content/published/api/v1.1/items?limit=' + widget.numberofNewsperpage() + '&offset=' + offset + '&q=(type eq ' + JSON.stringify(widget.obcontenttype()) + ' AND language eq ' + JSON.stringify(languageCode) + ' AND fields.news_content co "' + widget.obSearchTerm() + '")&channelToken=' + channelToken;
                        }
                    } else {
                        if (queryconstructedArray.length > 0) {
                            var queryparams = queryconstructedArray.join('AND');
                            newsurl = OCECURL + '/content/published/api/v1.1/items?limit=' + widget.numberofNewsperpage() + '&offset=' + offset + '&q=(type eq ' + JSON.stringify(widget.obcontenttype()) + ' AND language eq ' + JSON.stringify(languageCode) + ' AND ' + queryparams + ')&channelToken=' + channelToken;
                        } else {
                            newsurl = OCECURL + '/content/published/api/v1.1/items?limit=' + widget.numberofNewsperpage() + '&offset=' + offset + '&q=(type eq ' + JSON.stringify(widget.obcontenttype()) + ' AND language eq ' + JSON.stringify(languageCode) + ')&channelToken=' + channelToken;
                        }
                    }
                    $.ajax({
                      url: newsurl,
                      type: 'GET',
                      dataType: 'json',
  
                      success: function(res) {
                          if (res.items.length > 0) {
                              ajaxRequests = [];
                              widget.obNoNewsFound('');
                              widget.generatePagination(res);
                              for (var a = 0; a < res.items.length; a++) {
                                  widget.getitemContent(res.items[a]);
                              }
  
                          } else {
                              if(widget.obSearchTerm()){
                                widget.obNoNewsFound(widget.resources().obnewsErrorText1 + ' - ' +widget.obSearchTerm() +' - ' + widget.resources().obnewsErrorText2);
                              }else{
                                 widget.obNoNewsFound(widget.resources().obnewsErrorText1 + ' ' + widget.resources().obnewsErrorText2);
                              }
                             
                              widget.destroySpinner();
                          }
                      },
                      error: function(error) {
                        if(widget.obSearchTerm()){
                            widget.obNoNewsFound(widget.resources().obnewsErrorText1 + ' - ' +widget.obSearchTerm() + ' - ' +  widget.resources().obnewsErrorText2);
                          }else{
                             widget.obNoNewsFound(widget.resources().obnewsErrorText1 + widget.resources().obnewsErrorText2);
                          }
                          widget.destroySpinner();
                      }
                  });
                }
            },
            /** Method used to Get the contents of each Item */
            getitemContent: function(itemsdata) {
                responsecounter = 0;
  
                function configCall(data) {
                    var def = $.Deferred();
                    var ajaxRequestObj = [];
                    if (itemsdata.links[0].href) {
                        ajaxRequestObj = $.ajax({
                            url: itemsdata.links[0].href,
                            method: "GET"
                        });
                    }
  
                    ajaxRequests.push(ajaxRequestObj);
                    $.when.apply($, ajaxRequests).done(function() {
                        responsecounter++;
                        if (ajaxRequests.length == responsecounter) {
                            def.resolve(arguments);
                        }
  
                    });
                    return def.promise();
                }
                configCall(itemsdata).done(function(response) {
                    if (response[0].hasOwnProperty('fields')) {
                        response[0].news_large_image_url = ko.observable();
                        response[0].news_medium_image_url = ko.observable();
                        if (response[0].fields.news_image) {
                            response[0].news_large_image_url = OCECURL + "/content/published/api/v1.1/assets/" + response[0].fields.news_image.id + "/Large/" + response[0].fields.news_image.name + "?format=jpg&type=responsiveimage&channelToken=" + channelToken;
                            response[0].news_medium_image_url = OCECURL + "/content/published/api/v1.1/assets/" + response[0].fields.news_image.id + "/Medium/" + response[0].fields.news_image.name + "?format=jpg&type=responsiveimage&channelToken=" + channelToken;
                        } else {
                            response[0].news_large_image_url = '/file/general/no-image.png';
                            response[0].news_medium_image_url = '/file/general/no-image.png';
                        }
                        getWidget.obContentItems.push(response[0]);
                        getWidget.destroySpinner();
                        if(getWidget.obContentItems().length>0){
                             $.Topic('newsResults.memory').publish();
                        }
                    } else {
                        for (var r = 0; r < response.length; r++) {
                            response[r][0].news_large_image_url = ko.observable();
                            response[r][0].news_medium_image_url = ko.observable();
                            if (response[r][0].fields.news_image) {
                                response[r][0].news_large_image_url = OCECURL + "/content/published/api/v1.1/assets/" + response[r][0].fields.news_image.id + "/Large/" + response[r][0].fields.news_image.name + "?format=jpg&type=responsiveimage&channelToken=" + channelToken;
                                response[r][0].news_medium_image_url = OCECURL + "/content/published/api/v1.1/assets/" + response[r][0].fields.news_image.id + "/Medium/" + response[r][0].fields.news_image.name + "?format=jpg&type=responsiveimage&channelToken=" + channelToken;
                            } else {
                                response[r][0].news_large_image_url = '/file/general/no-image.png';
                                response[r][0].news_medium_image_url = '/file/general/no-image.png';
                            }
  
                            getWidget.obContentItems.push(response[r][0]);
                            getWidget.destroySpinner();
                        }
                        if(getWidget.obContentItems().length>0){
                            $.Topic('newsResults.memory').publish(true);
                        }
                    }
  
                });
            },
            /* Generates Pagination for news listing content */
            generatePagination: function(data) {
                var widget = getWidget;
                if (data.offset == 0) {
                    getWidget.obpagination([]);
                    var totalnumber = data.limit;
                    var itemperpage = widget.numberofNewsperpage();
                    var paginationlength = totalnumber / itemperpage;
                    for (var p = 0; p < paginationlength; p++) {
                        widget.obpagination.push(p + 1);
                    }
                    widget.obcurrentpage(1);
                    widget.obtotalNumberOfPages(widget.obpagination().length);
                } else {
                    return;
                }
            },
             /** Method to scroll top **/
             ob_scrollTop: function(){
                var $target = $('html,body');
				$target.animate({
					scrollTop: '0'
				}, 1000);
            },
            /** Method used to trigger when  first page is clicked  */
            obgotoFirstPage: function(data, event) {
                var widget = getWidget;
                /** fetch details of first page and set current page as 1 */
                widget.obContentItems([]);
                widget.fetchNewsDetails(0);
                widget.obcurrentpage(1);
                widget.ob_scrollTop();
            },
            /** Method used to trigger when  pervious page is clicked  */
            obgotoPreviousPage: function(data, event) {
                var widget = getWidget;
                var PrevOffset = widget.obcurrentpage() - 1;
                var offsetValue = (PrevOffset - 1) * widget.numberofNewsperpage();
                widget.obContentItems([]);
                widget.fetchNewsDetails(offsetValue);
                widget.obcurrentpage(data);
                widget.ob_scrollTop();
            },
            /** Method used to trigger when  current page is clicked  */
            obgotoClickedPage: function(data, event) {
                var widget = getWidget;
                if (widget.obcurrentpage() !== data) {
                    var offsetValue = (data - 1) * widget.numberofNewsperpage();
                    widget.obContentItems([]);
                    widget.fetchNewsDetails(offsetValue);
                    widget.obcurrentpage(data);
                } else {
                    return;
                }
                widget.ob_scrollTop();
            },
            /** Method used to trigger when  Next  page is clicked  */
            obgoToNextPage: function(data, event) {
                var widget = getWidget;
                var Nextpage = widget.obcurrentpage() + 1;
                var offsetValue = (Nextpage - 1) * widget.numberofNewsperpage();
                widget.obContentItems([]);
                widget.fetchNewsDetails(offsetValue);
                widget.obcurrentpage(Nextpage);
                widget.ob_scrollTop();
            },
            /** Method used to trigger when  last page is clicked  */
            obgoToLastPage: function(data, event) {
                var widget = getWidget;
                var lastbeforepage = widget.obtotalNumberOfPages();
                var offsetValue = (lastbeforepage - 1) * widget.numberofNewsperpage();
                widget.obContentItems([]);
                widget.fetchNewsDetails(offsetValue);
                widget.obcurrentpage(widget.obtotalNumberOfPages());
                widget.ob_scrollTop();
            },
            /* Method is trigger when coming from news landing page */
            switchtoNews: function(filterobj) {
  
                $.Topic('switchToNews').publish(filterobj.filtervalues);
                isBackfromNewsLanding = true;
                getWidget.obSearchTerm(filterobj.searchText);
               getWidget.constructsearchparamsforFliters(filterobj.filtervalues);
                getWidget.isNewsCategory(true);
            },
  
            beforeAppear: function(page) {
                var widget = this;
                queryconstructedArray = [];
                widget.getcurrentpage(page.path);
                widget.obNoNewsFound('');
                widget.obSearchTerm('');
                languageCode = getWidget.site().extensionSiteSettings.externalSiteSettings.langCode;
                channelToken = getWidget.site().extensionSiteSettings.externalSiteSettings.channelToken;
                OCECURL = getWidget.site().extensionSiteSettings.externalSiteSettings.OCECServerURL;
                widget.obContentItems([]);
                widget.isNewsCategory(false);
                if (document.cookie.indexOf('obnewsfiltersCookie') > -1) {
                    var obnewsfiltersCookie = JSON.parse(storageApi.getInstance().readFromCookies("obnewsfiltersCookie")); 
                    if (obnewsfiltersCookie.currentpage == page.path) {
                        widget.switchtoNews(obnewsfiltersCookie);
                    }else{
                      if (page.path == "news") {
                          widget.obContentItems([]);
                          widget.fetchNewsDetails(0);
                          widget.isNewsCategory(true);
                      }
                    }
                }else{
                      if (page.path == "news") {
                          widget.obContentItems([]);
                          widget.fetchNewsDetails(0);
                          widget.isNewsCategory(true);
                      }
                }
                
            }
        }
    }
  );