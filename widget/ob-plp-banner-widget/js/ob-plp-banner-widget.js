/**
 * @fileoverview plp banner Widget.
 *
 * @author Mastek
 */
define(
  //--------------------------------------- ----------------------------
  // DEPENDENCIES
  //-------------------------------------------------------------------

  ['knockout', 'pubsub','CCi18n'],

  //-------------------------------------------------------------------
  // MODULE DEFINITION
  //-------------------------------------------------------------------

  function(ko,pubsub,CCi18n) {

    "use strict";

    return {
   

      /*
       * Note that "this" is bound to the Widget View Model.
       */      
      resourcesLoaded : function(widget) {
      },

      onLoad : function(widget) { 
      },

      beforeAppear : function(page) {
      }
    }
  }
);
