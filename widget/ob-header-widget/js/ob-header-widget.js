/**
 * @fileoverview Mega Menu Widget.
 *
 */
define(
	//-------------------------------------------------------------------
	// DEPENDENCIES
	//-------------------------------------------------------------------
	['jquery', 'js/jquery.multilevelpushmenu', 'knockout', 'storageApi'],
	//-------------------------------------------------------------------
	// MODULE DEFINITION
	//-------------------------------------------------------------------
	function ($, multilevelpushmenu, ko, storageApi) {

		"use strict";
		var getwidget = '';
		return {
			catalogId: ko.observable(),
			maxNoOfElements: ko.observable(),
			ob_secondlevelChildCategories: ko.observable(''),
			ob_secondlevelChildCategoryImages: ko.observable(''),
			categories: ko.observableArray(),
			ob_Mobile: ko.observable(false),
			koMenuModelData: ko.observableArray(),
			getpageID: ko.observable(),
			/* languages select box functionality  */
			region: ko.observableArray([]),
			selectedRegion: ko.observable(),
			oB_regionLanguage: ko.observable(),
			oB_defaultRegion: ko.observable(),
			oB_countriesName: ko.observable(),
			oB_selectedCountry: ko.observable(),
			selectedRegionMobile: ko.observable(),
			oB_showNotification: ko.observable(),

			/** Update data for megamenu **/
			updateData: function (getData) {
				ko.mapping.fromJS(getData, {}, this.koMenuModelData);
			},

			/** Code to replace site in URL **/
			addReplaceLangCode: function (url, langCode) {
				var paths = [];
				var a = document.createElement('a');
				a.href = document.location.href;
				paths = a.pathname.split('/') ? a.pathname.split('/') : [];
				if (paths.length != 0) {
					//console.log("came in",paths[0] != "");
					paths.shift();
					if (paths[0]) {
						if (paths[0].length == 2 || paths[0].length === 5) {
							paths[0] = langCode;
						} else {
							paths.unshift(langCode);
						}
					} else {
						paths.unshift(langCode);
					}
				} else {
					paths.push(langCode);
				}

				return a.protocol + '//' +
					a.host + '/' + paths.join('/') +
					(a.search !== '' ? a.search : '') +
					(a.hash !== '' ? a.hash : '');
			},

			/** Parse string **/
			isJson: function (str) {
				try {
					JSON.parse(str);
				} catch (e) {
					return false;
				}
				return true;
			},

			/** Update countries name functionality for mobile **/
			oB_updateLeftNav: function (getdata) {
				var self = this;
				var resources = {
					"values": [{
							"countryName": getdata.resources().northamericatitletext,
							"countryAddress": getdata.resources().NAaddresstext,
							"countryEmail": getdata.resources().NAemailtext,
							"countryPhoneNo": getdata.resources().NAphonenotext
						},
						{
							"countryName": getdata.resources().southamericatitletext,
							"countryAddress": getdata.resources().SAaddresstext,
							"countryEmail": getdata.resources().SAemailtext,
							"countryPhoneNo": getdata.resources().SAphonenotext
						},
						{
							"countryName": getdata.resources().europetitletext,
							"countryAddress": getdata.resources().EUaddresstext,
							"countryEmail": getdata.resources().EUemailtext,
							"countryPhoneNo": getdata.resources().EUphonenotext
						},
						{
							"countryName": getdata.resources().asiatitletext,
							"countryAddress": getdata.resources().ASaddresstext,
							"countryEmail": getdata.resources().ASemailtext,
							"countryPhoneNo": getdata.resources().ASphonenotext
						},
						{
							"countryName": getdata.resources().africatitletext,
							"countryAddress": getdata.resources().AFaddresstext,
							"countryEmail": getdata.resources().AFemailtext,
							"countryPhoneNo": getdata.resources().AFphonenotext
						},
						{
							"countryName": getdata.resources().australiatitletext,
							"countryAddress": getdata.resources().AUaddresstext,
							"countryEmail": getdata.resources().AUemailtext,
							"countryPhoneNo": getdata.resources().AUphonenotext
						},
						{
							"countryName": getdata.resources().antarticatitletext,
							"countryAddress": getdata.resources().ANaddresstext,
							"countryEmail": getdata.resources().ANemailtext,
							"countryPhoneNo": getdata.resources().ANphonenotext
						}
					]
				};
				self.oB_countriesName(resources.values);
				self.oB_updateOnClick(resources.values[0]);
			},

			/** getting data on click and pubsub functionality for mobile **/
			oB_updateOnClick: function (newData, index) {
				var self = this;
				if (newData) {
					self.oB_selectedCountry(newData);
					$.Topic('SELECTED_COUNTRY.memory').publish(self.oB_selectedCountry());
				}
			},

			/** Store selected Region in cookie **/
			regionSelector: function (data, event) {
				var cookie = '';
				var obsitecookie = '';
				var userPreferred = '';
				var url;
				var currentSiteurl = window.location.pathname.split('/').length > 1 ? window.location.pathname.split('/')[1] : '';

				obsitecookie = storageApi.getInstance().readFromCookies("obsitecookie");
				if (getwidget.isJson(obsitecookie)) {
					if (obsitecookie) {
						obsitecookie = JSON.parse(obsitecookie);
						if (obsitecookie.userselected !== data.languagehref) {
							obsitecookie.userselected = data.languagehref;
							url = getwidget.addReplaceLangCode(window.location.href, data.languagehref);
							storageApi.getInstance().saveToCookies("obsitecookie", JSON.stringify(obsitecookie), 30);
							window.location.href = url;
						}
					} else {
						obsitecookie = {
							userdefault: '',
							userselected: ''
						};
						obsitecookie.userselected = data.languagehref;
						storageApi.getInstance().saveToCookies("obsitecookie", JSON.stringify(obsitecookie), 30);
						url = getwidget.addReplaceLangCode(window.location.href, data.languagehref);
						window.location.href = url;
					}
				}

			},

			/* languages select box functionality  */
			updateselectbox: function (getData) {
				var self = this;
				self.region = [{
						regionName: "North America",
						regionLanguage: [{
							language: getData.resources().osbornsiteUSen,
							languagehref: 'en-US'
						}]
					},
					{
						regionName: 'Global',
						regionLanguage: [{
								language: getData.resources().osbornsiteen,
								languagehref: 'en'
							},
							{
								language: getData.resources().osbornsitede,
								languagehref: 'de'
							},
							{
								language: getData.resources().osbornsitesv,
								languagehref: 'sv'
							},
							{
								language: getData.resources().osbornsitefr,
								languagehref: 'fr'
							},
							{
								language: getData.resources().osbornsitees,
								languagehref: 'es'
							}
						]

					}
				]
				self.selectedRegion(getData.resources().osbornglobal + ' - ' + getData.resources().osbornsiteUSen);
				self.selectedRegionMobile(getData.resources().osbornsiteUSenmbl);
			},

			/** Set Selected Region in header **/
			setRegionLanguage: function (selectedLanguage) {
				if (selectedLanguage) {
					switch (selectedLanguage) {
						case 'en-US':
							getwidget.selectedRegion(getwidget.resources().osbornSiteUS + ' - ' + getwidget.resources().osbornsiteUSen);
							getwidget.selectedRegionMobile(getwidget.resources().osbornsiteUSenmbl);
							getwidget.oB_regionLanguage("US");
							break;
						case 'en':
							getwidget.selectedRegion(getwidget.resources().osbornglobal + ' - ' + getwidget.resources().osbornsiteen);
							getwidget.selectedRegionMobile(getwidget.resources().osbornsiteenmbl);
							getwidget.oB_regionLanguage("Global-En");
							break;
						case 'de':
							getwidget.selectedRegion(getwidget.resources().osbornglobal + ' - ' + getwidget.resources().osbornsitede);
							getwidget.selectedRegionMobile(getwidget.resources().osbornsitedembl);
							getwidget.oB_regionLanguage("German");
							break;
						case 'sv':
							getwidget.selectedRegion(getwidget.resources().osbornglobal + ' - ' + getwidget.resources().osbornsitesv);
							getwidget.selectedRegionMobile(getwidget.resources().osbornsitesvmbl);
							getwidget.oB_regionLanguage("Swedish");
							break;
						case 'fr':
							getwidget.selectedRegion(getwidget.resources().osbornglobal + ' - ' + getwidget.resources().osbornsitefr);
							getwidget.selectedRegionMobile(getwidget.resources().osbornsitefrmbl);
							getwidget.oB_regionLanguage("French");
							break;
						case 'es':
							getwidget.selectedRegion(getwidget.resources().osbornglobal + ' - ' + getwidget.resources().osbornsitees);
							getwidget.selectedRegionMobile(getwidget.resources().osbornsiteesmbl);
							getwidget.oB_regionLanguage("Spanish");
							break;
					}
				}

			},

			/** Redirect to default site **/
			redirecttoDefault: function (data) {
				var url;
				var obsitecookie = storageApi.getInstance().readFromCookies("obsitecookie");
				obsitecookie = JSON.parse(obsitecookie);
				switch (data.oB_defaultRegion()) {
					case 'US':
						obsitecookie.userselected = 'en-US';
						break;
					case 'Global-EN':
						obsitecookie.userselected = 'en';
						break;
					case 'German':
						obsitecookie.userselected = 'de';
						break;
					case 'Swedish':
						obsitecookie.userselected = 'sv';
						break;
					case 'French':
						obsitecookie.userselected = 'fr';
						break;
					case 'Spanish':
						obsitecookie.userselected = 'es';
						break;
				}
				storageApi.getInstance().saveToCookies("obsitecookie", JSON.stringify(obsitecookie), 30);
				url = getwidget.addReplaceLangCode(window.location.href, obsitecookie.userselected);
				window.location.href = url;
			},  
  

			/** show level2 catefories **/
			oB_showSecondlevelCatergories: function (data, event) {
				var widget = this;
				$('.category-header').removeClass('active');
				$(event.currentTarget).addClass('active');
				if (data.hasOwnProperty('childCategories')) {
					$('.mega-menu-fullwidth').addClass('ob_staticPos');
					widget.ob_secondlevelChildCategories(data.childCategories());
					if (data.ob_categoryImage()) {
						widget.ob_secondlevelChildCategoryImages('/file/collections/' + data.ob_categoryImage() + '.png');
					} else {
						widget.ob_secondlevelChildCategoryImages('/file/general/no-image.png');
					}

				}
				if ((data.repositoryId() === 'LR001') || (data.repositoryId() === 'USLR001')) {
					$('.secondLevelCategory a').addClass('hide');
				} else {
					$('.secondLevelCategory a').removeClass('hide');
				}
			},

			/** hide level2 categories **/
			oB_hideSecondlevelCatergories: function (data) {
				var widget = this;
				$('.category-header').removeClass('active');
				if (data.hasOwnProperty('childCategories')) {
					$('#category-header0').addClass('active');
					$('.mega-menu-fullwidth').addClass('ob_staticPos');
					widget.ob_secondlevelChildCategories(data.childCategories());
					if (data.ob_categoryImage()) {
						widget.ob_secondlevelChildCategoryImages('/file/collections/' + data.ob_categoryImage() + '.png');
					} else {
						widget.ob_secondlevelChildCategoryImages('/file/general/no-image.png');
					}
				} else {
					$('.mega-menu-fullwidth').removeClass('ob_staticPos');
					widget.ob_secondlevelChildCategories('');
				}
			},


			onLoad: function (widget) {
				widget.oB_updateLeftNav(widget);
				getwidget = widget;
				widget.updateselectbox(widget);
				widget.menuName = 'CC-CategoryNav';
				var obsitecookie = '';
				obsitecookie = storageApi.getInstance().readFromCookies("obsitecookie");
				if (obsitecookie) {
					obsitecookie = JSON.parse(obsitecookie);
					if (obsitecookie.userselected) {
						widget.setRegionLanguage(obsitecookie.userselected);
					} else {
						widget.setRegionLanguage(obsitecookie.userdefault);
					}

					if (obsitecookie.userselected && obsitecookie.userdefault) {
						if (obsitecookie.userdefault !== obsitecookie.userselected) {
							widget.oB_showNotification(true);
							/**  not Getting the Dom While loading the page */
							setTimeout(function () {
								$('.oB_notification').show();
							}, 2000);
							setTimeout(function () {
								$('.oB_notification').hide();
							}, 30000);

							switch (obsitecookie.userdefault) {
								case 'en-US':
									widget.oB_defaultRegion("US");
									break;
								case 'en':
									widget.oB_defaultRegion("Global-EN");
									break;
								case 'de':
									widget.oB_defaultRegion("German");
									break;
								case 'sv':
									widget.oB_defaultRegion("Swedish");
									break;
								case 'fr':
									widget.oB_defaultRegion("French");
									break;
								case 'es':
									widget.oB_defaultRegion("Spanish");
									break;
							}
						}
					}

				}

				$(document).on('mouseover', 'li.cc-desktop-dropdown', function () {
					$(this).children('.dropdown-toggle').addClass('addBorderBottom');
				});

				$(document).on('mouseout', 'li.cc-desktop-dropdown', function () {
					$(this).children('.dropdown-toggle').removeClass('addBorderBottom');
				});

				/** Trigger search on click **/
				$("body").delegate("#searchSubmit", "click", function () {
					$('.ob_search').addClass('displaySearch');
					$('.input-group.search').addClass("searchBox");
					document.getElementById("CC-headerWidget-Search").focus();
				});

				/** Hide search box on focusout **/
				$("body").delegate("#CC-headerWidget-Search", "focusout", function () {
					$('.input-group.search').removeClass("searchBox");
				});

				/** Hide search box **/
				$(document).click(function (event) {
					if (!$(event.target).closest("#searchSubmit,.searchBox").length) {
						$("body").find(".searchBox").removeClass("searchBox");
					}
				});

				if (widget.user() != undefined && widget.user().catalogId) {
					widget.catalogId(widget.user().catalogId());
				}

				/** get categories **/
				var catalogId = widget.user().catalogId();
				widget.load('categoryList', ['rootCategory', catalogId, 1000], function (data1) {
					if ($.type(data1) == "object") {
						var tempData = [];
						tempData.push(data1);
						data1 = tempData;
					}
					widget.renderMobileMenuNew();
					widget.updateData(data1);
				});

				/** Show and hide mobile view code and trigger push menu close functionality**/
				$(window).resize(function () {
					$('.menu-close').trigger('click');
					if ($(window).width() <= 991) {
						widget.ob_Mobile(true);
					} else {
						widget.ob_Mobile(false);
					}
				});
				if ($(window).width() <= 991) {
					widget.ob_Mobile(true);
				} else {
					widget.ob_Mobile(false);
				}
			},

			beforeAppear: function (page) {
				var widget = this;
				widget.getpageID(page.pageId);
				/** Render push menu on page change**/
				$('body').removeClass("mp-pushed");
				$('.menu-close').trigger('click');
				widget.renderMobileMenuNew();
				if (page.path !== "searchresults" && page.path !== "news" && page.path !== "obNewsLandingPage") {
					// removing the cookie if it is not news page
					if (document.cookie.indexOf('obnewsfiltersCookie') > -1) {
						storageApi.getInstance().removeItem("obnewsfiltersCookie");
					}
				}
			},

			/** render push menu **/
			renderMobileMenuNew: function () {
				$('body').addClass("site-outer");
				$('body').removeAttr('style');
				setTimeout(function () {
					$('#mp-menu').pushMenu({
						type: 'overlap',
						levelSpacing: 0,
						backClass: 'mp-back',
						trigger: '.mobile-navbar-link',
						pusher: 'body',
						scrollTop: true
					});
				}, 500);
			}
		};
	});