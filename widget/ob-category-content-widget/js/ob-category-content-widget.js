/** fileoverview category center content  */
define(

  ['knockout', 'CCi18n'],

  function(ko, CCi18n) {

    "use strict";

    return {
      ob_categoryContent: ko.observable(),     
      resourcesLoaded : function(widget) {
      },

      onLoad : function(widget) { 
      },

      beforeAppear : function(page) {
          var widget = this;

          //To get the category from left nav
           $.Topic("getCategory.memory").subscribe(function(data) {
                  widget.ob_categoryContent(data);
              });
      }
    }
  }
);