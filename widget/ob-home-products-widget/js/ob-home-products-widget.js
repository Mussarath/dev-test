define(

  ['knockout','jquery','ccRestClient'],

  function(ko,$,ccRestClient) {
    "use strict";
    return {
     oB_products : ko.observable(''),
     oB_categories : ko.observableArray([]),
      resourcesLoaded : function(widget) {
      },

      onLoad : function(widget) { 
        // On window resizing this function will work 
        $( window ).resize(function() {
          widget.oB_resizeNewsEventsHeight();
        });   
        //Initialize Category Call
        widget.oB_getCategories();
      },

      /*
      * Get the Level 2 (Products Sub Level) Categories List
      */
     oB_getCategories : function(widget) {
         var widget=this,catalogId,siteId,l={};
                l['fields'] = 'childCategories,route';
                l['expand'] = 'childCategories';
                l['maxlevel'] = 1000;
                siteId = widget.site().extensionSiteSettings.externalSiteSettings.langCode;
                if((siteId === "en-US") || (siteId === "es")){
                  catalogId = 'USOBProducts'
                }else{
                  catalogId = 'obProducts'
                }
                ccRestClient.authenticatedRequest("/ccstoreui/v1/collections/" + catalogId ,l, function (data) {
                    if(data){
                         widget.oB_categories(data.childCategories);
                        widget.oB_products(data.route);
                    }
                }, function (err) {
                }, "GET");   
     },

       // finds the news and events div heights
      
       oB_resizeNewsEventsHeight: function() {
        var highestBox = 0;
        $('.marginBtm15 h4').each(function(){  
           
            if($(this).height() > highestBox){  
                highestBox = $(this).height();  
            }
        });    
        $('.marginBtm15 h4').height(highestBox);
            
      },
       // checks the container is visible on the page 
      checkContainer :function(){
        var widget = this;
          if($('.marginBtm15 h4').is(':visible')){  
            var evt = window.document.createEvent('UIEvents'); 
            evt.initUIEvent('resize', true, false, window, 0); 
            window.dispatchEvent(evt);
            
            widget.oB_resizeNewsEventsHeight();
            
          }
      },
      beforeAppear : function(page) {
        var widget = this;
        if(window.innerwidth >= 768) { 
             widget.checkContainer();
        }
 
      }
    }
  }
);