/**
 * @fileoverview Guided Navigation Widget. 
 * 
 */
define(

  //-------------------------------------------------------------------
  // DEPENDENCIES
  //-------------------------------------------------------------------
  ['knockout','viewModels/guidedNavigationViewModel', 'CCi18n',
  'ccConstants', 'pubsub'],

  //-------------------------------------------------------------------
  // MODULE DEFINITION
  //-------------------------------------------------------------------
  function(ko, GuidedNavigationViewModel, CCi18n, CCConstants, pubsub) {  
  
    "use strict";

    return {
      displayRefineResults: ko.observable(false),
      maxDimensionCount: ko.observable("10"),
      maxRefinementCount:ko.observable("10"),
      isSearchResults : ko.observable(false),
      isNewsCategory : ko.observable(false),
      /**
        Guided Navigation Widget.
        @private
        @name guided-navigation
        @property {observable GuidedNavigationViewModel} view model object representing the guided navigation details
       */
      onLoad: function(widget) {
          widget.isSearchResults(false);
          widget.isNewsCategory(false);
        /** This PubSub swap from  products to  News  */
          $.Topic('swapToNews').subscribe(function(v){
            widget.isNewsCategory(true); 
          });
          /** This PubSub swaps from  News  to  Products  */
          $.Topic('swapToProducts').subscribe(function(v){
            widget.isNewsCategory(false); 
          });
        /**
             * Converts link from new pattern to old patten.
             * @function GuidedNavigationViewModel#convertLabel
             */
            GuidedNavigationViewModel.prototype.toLegacyUrl = function(gsQueryUrl) {
              return  gsQueryUrl;
          };
        widget.guidedNavigationViewModel = ko.observable();
        widget.guidedNavigationViewModel(new GuidedNavigationViewModel(widget.maxDimensionCount(), widget.maxRefinementCount(), widget.locale()));
        
        $.Topic(pubsub.topicNames.SEARCH_RESULTS_FOR_CATEGORY_UPDATED).subscribe(function(obj){
          if (!this.navigation || this.navigation.length == 0) {
            widget.displayRefineResults(false);
          }
          else {
            widget.displayRefineResults(true);
          }
        });
        $.Topic('ObBacktoPlp').subscribe(function(){
            widget.isSearchResults(false);
        });
        $.Topic(pubsub.topicNames.SEARCH_FAILED_TO_PERFORM).subscribe(function(obj) {
          widget.displayRefineResults(false);
        });
        
        $.Topic(pubsub.topicNames.SEARCH_RESULTS_UPDATED).subscribe(function(obj) {
            widget.isSearchResults(true);
          if ((this.navigation && this.navigation.length > 0) || (this.breadcrumbs && this.breadcrumbs.refinementCrumbs.length > 0)) {
            widget.displayRefineResults(true);
          }
          else {
            widget.displayRefineResults(false);
          }
        });
      },
      beforeAppear:function(page){
        var widget = this;
        widget.isNewsCategory(false);
      },
    };
  }
);
