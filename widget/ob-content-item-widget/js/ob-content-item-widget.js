define(

  // -------------------------------------------------------------------
  // DEPENDENCIES
  // -------------------------------------------------------------------
    [ 'jquery', 'knockout', 'ccLogger', 'ccDate', 'ccConstants', 'ccRestClient' ],
  
  // -------------------------------------------------------------------
  // Module definition
  // -------------------------------------------------------------------
    function($, ko, CCLogger, CCDate, CCConstants, ccRestClient) {
      'use strict';
      return {
        content : ko.observableArray([]),
        info : ko.observable({}),
        enabled : false,
        onLoad : function(widget) {
          // get content settings
          var contentSettings = widget.site().extensionSiteSettings.ContentSettings;
  
          if (contentSettings && contentSettings.enabled === true) {
            if (contentSettings.content) {
              widget.serverUrl = contentSettings.content.url;
              widget.channelToken = contentSettings.content.accessToken;
              widget.apiPath = widget.site().oracleCEC && widget.site().oracleCEC.apiPath;
              widget.itemsMethod = widget.site().oracleCEC && widget.site().oracleCEC.itemsMethod;
            }
            widget.enabled = true;
          } else {
            // Content not set up
            widget.serverUrl = '';
            widget.channelToken = '';
          }
  
          // set up helper functions for content
  
          // Method to fetch digital asset
          widget.getRemoteItem = function(asset) {
            var url = asset, // default return value to be the original asset in case nothing can be found
              assetUrl = asset && asset.links && asset.links.length && asset.links[0].href, item = {};
  
            if (assetUrl) {
              $.ajax({
                url : assetUrl,
                async : false,
                type : asset && asset.links && asset.links.length && asset.links[0].method || 'GET',
                /*beforeSend: function (xhr){
                  xhr.setRequestHeader('Authorization', 'Basic b2NjdXNlcjFAb3JhY2xlLmNvbTpBc2RmcG9pdTFAMzQ=');
                }, */
                headers: {
                  "Authorization": "Basic "+window.btoa('karthik.hari@mastek.com' + ":" + 'K@rthik4osborn')
                },
                success : function(response) {
                  if (response && response.fields) {
  
                    //check each of the items within data for more downloadable data
                    for ( var key in response.fields) {
                      if (response.fields[key].hasOwnProperty("links")) {
                        item[key] = widget.getRemoteItem(response.fields[key]);
                      } else {
                        item[key] = response.fields[key];
                      }
                    }
  
                    item.name = response.name;
                    item.description = response.description;
                    item.type = response.type;
                    item.createddate = response.createddate;
                    item.updateddate = response.updateddate;
                  }
                }
              });
            }
            return item;
          };
  
          // build content item
          widget.getContentItem = function(ele) {
            var contentItem = {};
            //if the item has a link object, need to download the content
            if (ele != null && typeof ele == 'object' && ele.hasOwnProperty("links")) {
              contentItem = widget.getRemoteItem(ele);
            } else if (ele != null) {
              contentItem = ele;
            }
            return contentItem;
          };
  
          /**
           Build URL, removing slashes from beginning and end
           */
          widget.buildUrl = function(cmsContentSettings){
            var url = "";
  
            url += widget.serverUrl.replace(/\/$/, "").replace(/^\//, "") + "/";
            url += cmsContentSettings.apiPath.replace(/\/$/, "").replace(/^\//, "") + "/";
            url += cmsContentSettings.itemsMethod.replace(/\/$/, "").replace(/^\//, "") + "/";
  
            return url;
          }
        },
  
        /**
         * Before the widget appears
         * @param page the page vm
         * @returns {boolean}
         */
        beforeAppear : function(page) {
          var widget = this, ele, content = {}, info = {},
              cmsContentMap, cmsContentSettings, url,
              assetId, channelToken, cacheBuster;
  
          widget.content("");
          widget.info("");
  
          cmsContentMap = widget.cmsContentMap();
          cmsContentSettings = widget.cmsContentSettings();
  
          if (!cmsContentSettings){
            //settings aren't retrieved with layout, so this is most likely a standalone widget
            cmsContentSettings = {};
            cmsContentSettings.apiPath = widget.apiPath;
            cmsContentSettings.itemsMethod = widget.itemsMethod;
          }
  
          if (!widget.enabled) {
            return false;
          }
  
          // Build API Url - remove any trailing slash
          url = widget.buildUrl(cmsContentSettings);
  
         /* if (widget.assetId && widget.assetId()){
            assetId = widget.assetId();
          } else {
            assetId = cmsContentMap.contentId || '';
          } */

          assetId = 'CORE4AAAA0911D594DB889BCC0D8B645DDBF';
  
          channelToken = widget.channelToken || '';
          cacheBuster = cmsContentMap && cmsContentMap.version || '1';
  
          // build URL - Integration setting should have full api url
          url += assetId + "?" + CCConstants.CEC_CHANNEL_TOKEN + "=" + channelToken + "&cb=" + cacheBuster;
  
          // *** TODO: append locale here ****
          //
  
          // TODO , remove the setRequestHeader when CEC remove the security on the call
          // get the item
          ccRestClient.request(
            url,
            null,
            function(response){ // success
              if (response && response.fields) {
  
                // define data field prefix, based on content "type"
                var prefix = response.type.toLowerCase();
  
                for (var key in response.fields) {
                  ele = response.fields[key];
                  if ($.isArray(ele)) {
                    // multiple digital assets
                    $.each(ele, function(i, element) {
                      content[key] = widget.getContentItem(element);
                    });
                  } else {
                    content[key] = widget.getContentItem(ele);
                  }
                }
                info = {};
                info.description = response.description;
                info.name = response.name;
                info.type = response.type;
                info.createddate = response.createdDate;
                info.updateddate = response.updatedDate;
                info.language = response.language;
                info.slug = response.slug;
  
                // format the date value part
                if (!info.updateddate){
                  info.updateddate = {};
                }
                info.updateddate.value = CCDate.formatDateAndTime(info.updateddate.value, null, null, "long");
  
                widget.content(content);
                widget.info(info);
              }
            },
            function(response) {
              // failure
              CCLogger.warn("Failed to get CEC content item.", response);
            },
            null,
            null,
            null,
            null,
            function(xhr) {
              xhr.setRequestHeader('Authorization', 'Basic b2NjdXNlcjFAb3JhY2xlLmNvbTpBc2RmcG9pdTFAMzQ=');
            }
          );
          //Content Item Custom call
          widget.obItemRequest();
        },
        obItemRequest: function() {
          var widget = this, ele, content = {}, info = {};

          $.ajax({
            url: 'https://osborn-jasoninc.uscom-east-1.oraclecloud.com/content/published/api/v1.1/items/CORE4AAAA0911D594DB889BCC0D8B645DDBF?channelToken=132139082e402d69520da44f5ade7c85',
            type: 'GET',
            dataType: 'json',
            
            success: function(data){
                console.log('------ Integrated Data------', data);
                if(data.fields && data.fields.news_image && data.fields.news_image.links && data.fields.news_image.links.length > 0){
                  $.ajax({
                    url: data.fields.news_image.links[0].href,
                    type: 'GET',
                    dataType: 'json',
                    
                    success: function (imageData) { 
                      console.log('Image Data:', imageData);
                     },
                     error: function (err) { 
                       console.log('Error:', err);
                     }
                  });
                }
                //OOTB Success
                var response = data;
                if (response && response.fields) {
  
                  // define data field prefix, based on content "type"
                  var prefix = response.type.toLowerCase();
    
                  for (var key in response.fields) {
                    ele = response.fields[key];
                    if ($.isArray(ele)) {
                      // multiple digital assets
                      $.each(ele, function(i, element) {
                        content[key] = widget.getContentItem(element);
                      });
                    } else {
                      content[key] = widget.getContentItem(ele);
                    }
                  }
                  info = {};
                  info.description = response.description;
                  info.name = response.name;
                  info.type = response.type;
                  info.createddate = response.createdDate;
                  info.updateddate = response.updatedDate;
                  info.language = response.language;
                  info.slug = response.slug;
    
                  // format the date value part
                  if (!info.updateddate){
                    info.updateddate = {};
                  }
                  info.updateddate.value = CCDate.formatDateAndTime(info.updateddate.value, null, null, "long");
    
                  widget.content(content);
                  widget.info(info);
                }

            },
            error: function(err){

            }
          });


        }
      };
    });
  