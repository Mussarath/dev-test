/** fileoverview show product details */
define(

  ['knockout', 'CCi18n', 'pubsub', 'ccRestClient'],

  function(ko, CCi18n , pubsub, ccRestClient ) {

    "use strict";

    return {
      ob_productData: ko.observableArray([]),
      resourcesLoaded : function(widget) {
      },

      onLoad : function(widget) {
      },
      
      //Get Product Data
      ob_getProductData: function(){
        var widget = this;
        var getURL = window.location.pathname;
          if(getURL.split('/').length>3){
            var getProductID = getURL.split('/')[4];
          }
        var l = {};
        l['fields']  = 'items.id,items.displayName,items.mediumImageURLs,items.longDescription';
        ccRestClient.authenticatedRequest("/ccstoreui/v1/products?categoryId=" + getProductID , l , function (data) {            
            if(data.items.length>0){
                widget.ob_productData(data.items);
            }
        }, function (data) {}, "GET");
      },

      beforeAppear : function(page) {
           var widget = this;
           //Initiate Get Product data call
           widget.ob_getProductData();                     
      }
    }
  }
);