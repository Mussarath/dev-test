/**
 * @fileoverview Source file for gdpr cookie
 * 
 */
define(

  ['knockout', 'jquery', 'CCi18n', 'navigation', 'storageApi'],

    function(ko, $, CCi18n, n, storageApi) {
var gdprcookie;

    "use strict";

    return {
        
        gdprBanner: ko.observable(true),
      /*
       * Note that "this" is bound to the Widget View Model.  
       */      
      resourcesLoaded : function(widget) {
      },

      onLoad : function(widget) { 
      },

    /**start gdpr cookie functionality **/
    ob_acceptCookie: function(data){
        var widget = this;
        /** save to cookie **/
        gdprcookie  = storageApi.getInstance().saveToCookies("obgdprcookie", JSON.stringify("true"), 30);
         $(".ob-home-gdpr-banner-widget").delay("slow").fadeOut();   
    },
    /**End gdpr cookie functionality **/
    
      beforeAppear : function(page) {
        var widget = this;
        /** remove from cookie **/
         var ob_gdprcookie = storageApi.getInstance().readFromCookies("obgdprcookie");
         
         /** show banner if browser has cookie **/
        if(ob_gdprcookie){
            widget.gdprBanner(false);
        }  
        else{
            widget.gdprBanner(true);  
        }  
      }
    }
  }
);