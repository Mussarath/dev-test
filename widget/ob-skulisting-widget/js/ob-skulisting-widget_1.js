
/*** fileoverview for sku attributes list ***/
define(

	['jquery', 'knockout', 'ccRestClient', 'storageApi', 'notifier','js/pdfmake', 'js/jquery.dataTables'],

	function ($, ko, ccRestClient, storageApi,notifier) {

		"use strict";
		var table;
		var oTable;
		var numberofItemsadded;
		var selectedFilter = [];
		var filteredVal, filteredTxt;

		var obPdfProductImage = "";
        var obPdfAdditionalSealImage = [];
		var obAvailableInBothCatalog = false;
		var obCatalog = 'all';
		var obOsbornLogo = "";
		/** remove html elements in Data table header */
    	 var removeElements = function(text, selector) {
    		var wrapped = $("<div>" + text + "</div>");
    		wrapped.find(selector).remove();
    		return wrapped.html();
    	};
    	
    	/** convert image to base64 for PDF download */
    	var getBase64ImageFromURL = function(url) {
    		return new Promise(function(resolve, reject) {
    		  var img = new Image();
    		  img.setAttribute("crossOrigin", "anonymous");
    		  img.onload = function()  {
    			var canvas = document.createElement("canvas");
    			canvas.width = img.width;
    			canvas.height = img.height;
    			var ctx = canvas.getContext("2d");
    			ctx.drawImage(img, 0, 0);
    			var dataURL = canvas.toDataURL("image/png");
    			canvas = null;
    			resolve(dataURL);
    		  };
    		  img.onerror = function(error) {
    			reject(error);
    		  };
    		  img.src = url;
    		});
		  }
		  
		return {
			ob_skuattributes: ko.observableArray([]),
			ob_wishListArray: ko.observableArray([]),
			ob_uniqueWishlists: ko.observable(),
			ob_selectedSKU: ko.observableArray([]),
			ob_categoryProductHeaders : ko.observableArray([]),
			resourcesLoaded: function (widget) {},

			onLoad: function (widget) {

			},
			ob_datatable: function () {
				var widget = this;
				/** Get column headers **/
				var header = [];
				var columndata = [];
				var data = widget.product().childSKUs();
				var skulength = widget.product().skuProperties().length;
				
				if(widget.product().Ob_ProductCatalogTableHeaders()){
				    	var productHeaderids =[];
					if(widget.product().Ob_ProductCatalogTableHeaders().indexOf(';') > -1){
			    	productHeaderids = widget.product().Ob_ProductCatalogTableHeaders().indexOf(';') > -1 ? widget.product().Ob_ProductCatalogTableHeaders().split(';') : [];

					}else if (widget.product().Ob_ProductCatalogTableHeaders().indexOf(',') > -1){
						productHeaderids = widget.product().Ob_ProductCatalogTableHeaders().indexOf(',') > -1 ? widget.product().Ob_ProductCatalogTableHeaders().split(',') : [];
					}
					for(var i =0;i<productHeaderids.length;i++){
						for(var j=0;j<skulength;j++){
							if(productHeaderids[i].indexOf('ob_') > -1){
								if(productHeadersid[i] == widget.product().skuProperties()[j].id){
									columndata.push({
										'data': productHeaderids[i]
									});
								    header.push(widget.product().skuProperties()[j].label);
								    break;
								}
							}else{
								if(widget.product().skuProperties()[j].id.indexOf(productHeaderids[i]) > -1){
									columndata.push({
										'data': ko.toJS(widget.product().skuProperties()[j].id)
									});
								    header.push(ko.toJS(widget.product().skuProperties()[j].label));
								    break;
								}
							}
						}
						
					}
					
				}else{
					for (var i = 0; i < skulength; i++) {
						for (var key in data[0]) {
							if (key == widget.product().skuProperties()[i].id) {
								if (((typeof data[0][key] != 'function') && (data[0][key] != null) && (data[0][key].length > 0)) || ((typeof data[0][key] == 'function') && (data[0][key]() != null) && (data[0][key]().length > 0))) {
									columndata.push({
										'data': key
									});
								    header.push(widget.product().skuProperties()[i].label);
								}
							}
						}
					}
				}
				widget.ob_skuattributes(header);
				columndata.push({
					render: function (data, type, src, meta) {
					    var showbuynow = (src.Ob_buyNowAvailable())?'<button class="cswidget" data-asset-id="119" data-product-sku= ' + src.Ob_ItemNumber() + '><img src="/file/general/ob-buy-now.svg" class="buynow" title="Buy Now" alt="Buy Now"></button>':'';
						return '<a><img src="/file/general/ob-wishlist-icon.svg" alt="Add to Wishlist" title="Add to Wishlist" class="addwishlist"></a>'+showbuynow;
					}
				});

				/** show dropdown list for filter */
				function cbDropdown(column) {
					return $('<ul>', {
						'class': 'cb-dropdown'
					}).appendTo($('<div>', {
						'class': 'cb-dropdown-wrap',
						'style': 'display:none'
					}).appendTo(column));
				}
                
                /** Show selected filters below header label **/
				function cbfilter(column) {
					var cbfilter = $('<div>', {
						'class': 'filterSKUvalues'
					});
					return cbfilter.appendTo(column);
				}

                /** Show filter text below header label **/
				function filter(column) {
					var 
						$filterwrapper = $('<div>'),
						$plus = $('<span>', {
							'class': 'glyphicon glyphicon-plus'
						}),
						$filter = $('<span>', {
							'class': 'filter',
							text: widget.resources().obfilter
						});

					$plus.appendTo($filterwrapper);
					$filter.appendTo($filterwrapper);
					return $filterwrapper.appendTo(column);
				}

                /**Show dropdown arrow near lable **/
				function dropdownarrow(column) {
					var dropdownarrow = $('<span class="caret"></span>');
					return dropdownarrow.appendTo(column);
				}


				/** start Datatable code **/
				var column = columndata;
				var pdfPageSize = "A4";
				
				if(columndata.length > 12) {
				    pdfPageSize = "A1";
				} else if(columndata.length > 8) {
				    pdfPageSize = "A2";
				} else if(columndata.length > 6) {
					
				    pdfPageSize = "A3";
				}
				//console.log('data=========>',data);
				oTable = $('#skuTable').dataTable({
					"ordering": false,
					"paging": false,
					data: data,
					columns: column,
					dom: 'Bfrtip',
					info: false,
					pagingType: "full_numbers",
					language: {
                        "paginate": {
                          "previous": "<",
                          "next": ">",
                          "first": "<<",
                          "last": ">>"
                        }
                    },
                    buttons: [
						{
                            extend: 'csvHtml5',
                            text: 'Export Data',
                            title: widget.product().displayName(),
                            customize: function (csv) {
        						if (csv) {
        						    //console.log("csv.....", csv);
        						    var split_csv = csv.split("\n");
        						    
        						    var csv_header = split_csv[0];
        						   
        						    var split_csv_header = csv_header.split(",");
        						    //console.log("split_csv_header.....", split_csv_header);
        						    
        						    var newHeader = [];
        						    $.each(split_csv_header, function (index, item) { 
        						        //console.log("split_csv_header item.....", item);
        						        var removeQuote = item.substring(1, item.length-2);
										var item_split = "";
                                        if(removeQuote.indexOf(widget.resources().obactivefilter) !=-1) {
                                            item_split = removeQuote.split(widget.resources().obactivefilter);
                                        } else {
                                            item_split = removeQuote.split(widget.resources().obfilter);
                                        }
        						        newHeader.push(item_split[0]);
        						    })
        						    //console.log("newHeader.....", newHeader);
        						    split_csv[0] = newHeader.join(",");
        						    
        						    csv = split_csv.join("\n");
        						    //console.log("csv.....", csv);
        						    return csv;
        						}
                            },
                            exportOptions: {
                                modifier: {
                                    search: 'none'
                                }
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            text: 'PDF',
                            title: widget.product().displayName(),
                            defaultFileName :widget.product().id(),
                            pageSize: pdfPageSize,
                            customize: function (doc) {
        						if (doc) {
        						    doc.content.forEach(function(item) {
										//console.log(item);
										 if (item.table) {
										     item.table.widths =  Array(item.table.body[0].length + 1).join('*').split('');
										     item.margin = [0, 20, 20, 0];
											/** remove filter dropdown element from the table header */
											var itemHeader = item.table.body[0];
											itemHeader.forEach(function(headerContent) {
											    //console.log("headerContent.text........", headerContent.text);
												if(headerContent.text) {													
    												var removedHtmlString = "";
                                                    if(headerContent.text.indexOf(widget.resources().obactivefilter) !=-1) {
                                                        removedHtmlString = headerContent.text.substring(0, headerContent.text.indexOf(widget.resources().obactivefilter));
                                                    } else {
                                                        removedHtmlString = headerContent.text.substring(0, headerContent.text.indexOf(widget.resources().obfilter));
                                                    }

													headerContent.text = removedHtmlString;
													//console.log(headerContent.text);
												}
											});
											
											var tableBody = item.table.body;
											for(var a=1; a<tableBody.length; a++) {
											    for(var b=0; b<tableBody[a].length; b++) {
													tableBody[a][b].margin = [0, 0, 0, 0];
												}
											}
										 } 
									});
									
									/*** Product URL */
									doc.content.splice(1, 0, {
									  text: [{
										text: window.location.href+'\n',
										bold: false,
										fontSize: 14
									  }],
									  margin: [0, 0, 0, 0],
									  alignment: 'left'
									});
									
									
									var pdfRight = [];
									
									/** PDF Product Title */
									var pdfTitle = {};
									pdfTitle.text = widget.product().displayName()+'\n';
									pdfTitle.bold = true;
									pdfTitle.fontSize = 16;
									pdfTitle.margin = [0, 0, 0, 20];
									pdfRight.push(pdfTitle);
									
									/** PDF Product Description */
									if(widget.product().description()) {
										var pdfDescription = {};
										pdfDescription.text = widget.product().description()+'\n';
										pdfDescription.margin = [0, 0, 0, 20];
										pdfRight.push(pdfDescription);
									}

									/** PDF Product Marketing Description */
									if(widget.product().Ob_ItemMarketingDescription()) {
										var pdfMarketingDescription = {};
										pdfMarketingDescription.text = widget.product().Ob_ItemMarketingDescription()+'\n';
										pdfMarketingDescription.margin = [0, 0, 0, 20];
										pdfRight.push(pdfMarketingDescription);
									}
									
									
									/**** Product Advantages */
									var itemAdvantage = [];
									
									if(widget.product().Ob_ItemAdvantageBullet1()) {
									   itemAdvantage.push(widget.product().Ob_ItemAdvantageBullet1());
									}
									if(widget.product().Ob_ItemAdvantageBullet2()) {
									   itemAdvantage.push(widget.product().Ob_ItemAdvantageBullet2());
									}
									if(widget.product().Ob_ItemAdvantageBullet3()) {
									   itemAdvantage.push(widget.product().Ob_ItemAdvantageBullet3());
									}
									if(widget.product().Ob_ItemAdvantageBullet4()) {
									   itemAdvantage.push(widget.product().Ob_ItemAdvantageBullet4());
									}
									if(widget.product().Ob_ItemAdvantageBullet5()) {
									   itemAdvantage.push(widget.product().Ob_ItemAdvantageBullet5());
									}
									
									if(itemAdvantage.length > 0 ) {
									    var pdfAdvantage = {};
									    pdfAdvantage.ul = itemAdvantage;
									    pdfAdvantage.margin = [0, 0, 0, 20];
    									pdfRight.push(pdfAdvantage);
									}
									
									/**** Product additional seals */
									if(obPdfAdditionalSealImage.length > 0) {
									    //console.log("obPdfAdditionalSealImage.......", obPdfAdditionalSealImage);
									    
									    var pdfAdditionalSealImage = {};
									    pdfAdditionalSealImage.columns = [];
									    
									    for(var i=0; i<obPdfAdditionalSealImage.length; i++) {
									        var pdfAddSealImage = {};
        									pdfAddSealImage.image = obPdfAdditionalSealImage[i];
        									pdfAddSealImage.width = 50;
        									pdfAdditionalSealImage.columns.push(pdfAddSealImage);
									    }
									    pdfAdditionalSealImage.margin = [0, 0, 0, 20];
        								pdfRight.push(pdfAdditionalSealImage);
									}
									
									
									/**** Product Application text */
									if(widget.product().Ob_ProductApplicationText()) {
									    var pdfApplication = {};
									    pdfApplication.text = [];
									    
									    var pdfAppLabel = {};
									    pdfAppLabel.text = "Applications"+" : ";
									    pdfAppLabel.bold = true;
									    pdfApplication.text.push(pdfAppLabel);
									    
									    var pdfAppValue = {};
									    pdfAppValue.text = widget.product().Ob_ProductApplicationText();
									    pdfAppValue.bold = false;
									    pdfApplication.text.push(pdfAppValue);
									    
									    
									   pdfRight.push(pdfApplication);
									}
									
									
									
									//console.log("pdfRight.......", pdfRight);
									doc.content.splice(2, 0, {
									  columns: [
                                        {
                                          // auto-sized columns have their widths based on their content
                                          width: 200,
                                          image: obPdfProductImage
                                        },
                                        {
                                          width: '*',
                                          stack:pdfRight
                                        }
                                      ],
                                      // optional space between columns
                                      columnGap: 10,
                                      margin: [0, 20, 0, 0],                                          
                                      fontSize: 12
									});
									
									
									doc['styles'] = {
									    tableHeader: {
                        					fontSize: 10,
                        					color: '#000000',
                        					fillColor: '#e5e5e5',
                        					alignment: 'center',
                        					bold: true
                        				},
                                        tableBodyEven: {
                                            fillColor: '#f3f3f3',
                        					alignment: 'center'
                                        },
                        				tableBodyOdd: {
                        					fillColor: '#ffffff',
                        					alignment: 'center'
                        				},
                                    };
									//console.log("doc.......", doc);
        						}
                            },
                            exportOptions: {
                                
                            }
                        }
					],

					initComplete: function () {
					    var $buttons = $('.dt-buttons').hide();
        				$('#downloadDataExcel').on('click', function() {
        					$buttons.find(".buttons-csv").click();
        				});
        				$('#downloadDataPDF').on('click', function() {
        					$buttons.find(".buttons-pdf").click();
						});
						
					    /** Get Parameters from URL for filter**/
						selectedFilter = [];
						var getqueryparams = (window.location.search);
						if (getqueryparams) {
							if (getqueryparams.indexOf('&') !== -1) {
								var result = getqueryparams.split("&");
								for (var k = 0; k < result.length; k++) {
									filteredTxt = result[k].split("=");
									filteredVal = filteredTxt[1].split(",");
									selectedFilter.push(filteredVal);
								}
							} else {
								filteredTxt = getqueryparams.split("=");
								filteredVal = filteredTxt[1].split(",");
								selectedFilter.push(filteredVal);
							}
						}
						
						var table = $('#skuTable').DataTable();
						this.api().columns().every(function () {
							var column = this;
                            if($(column.header()).hasClass("skuimages")) {
                                return false;
                            }
							var ddarrow = dropdownarrow($(column.header()))
							var ddfilter = filter($(column.header()))
							var ddfilterselected = cbfilter($(column.header()))
							var ddmenu = cbDropdown($(column.header()))
								.on('change', ':checkbox', function () {
									$('body').find(".cb-dropdown-wrap").hide();
									$(document).on('click','input[type="checkbox"]',function(event){
										if (this.checked && ddmenu.children().children().children('input[type="checkbox"]:checked').length > 3){
										   event.preventDefault();
										}
								   });
									var active;
									widget.ob_selectedSKU([]);
									var vals = $(':checked', ddmenu).map(function (index, element) {
									    widget.ob_selectedSKU.push($(this).parent().parent().text());
										active = true;
										return $.fn.dataTable.util.escapeRegex($(element).val());
									}).toArray().join('|');

									column
										.search(vals.length > 0 ? '^(' + vals + ')$' : '', true, false)
										.draw();
										
										if($('body').find('.dataTables_empty').length>0){
										    if ($(window).width() <= 767) {
                                                $('#skuTable').height(($('#skuTable').children('thead').children('tr').children('td').length)*64);
										    }
										    else{
										        $('#skuTable').height('unset');
										    }
										    $(window).resize(function () {
										         if ($(window).width() <= 767) {
                                                $('#skuTable').height(($('#skuTable').children('thead').children('tr').children('td').length)*64);
										    }
										    else{
										        $('#skuTable').height('unset');
										    }
										    });
                            }

									// Highlight the current item if selected.
									if (this.checked) {
										$(this).closest('li').addClass('active');
									} else {
										$(this).closest('li').removeClass('active');
									}

									// Highlight the current filter if selected.
									var active2 = ddmenu.parent().is('.active');
									if (active && !active2) {
										ddmenu.parent().addClass('active');
										ddmenu.parent().parent().find('.filter').text(widget.resources().obactivefilter);
										ddmenu.parent().parent().find('.caret').addClass('activefilter');
										ddmenu.parent().parent().find('.glyphicon-plus').addClass('activefilter');
										ddmenu.parent().parent().find('.filter').addClass('activefilter');
									} else if (!active && active2) {
										ddmenu.parent().removeClass('active');
										ddmenu.parent().parent().find('.filter').text(widget.resources().obfilter);
										ddmenu.parent().parent().find('.caret').removeClass('activefilter');
										ddmenu.parent().parent().find('.glyphicon-plus').removeClass('activefilter');
										ddmenu.parent().parent().find('.filter').removeClass('activefilter')
									}

									/** Show filtered values below label **/
									ddmenu.parent().prev().text(widget.ob_selectedSKU()).addClass('filteredSKU');
									if ($('input[type="checkbox"]:checked').length > 0) {
										$('.filterSKUvalues').addClass('skulist');
									} else {
										$('.filterSKUvalues').removeClass('skulist');
									}
								    	if($('body').find('.activefilter').length > 0){
									    $('.cb-dropdown').addClass('filtered');
									}
									else{
									    $('.cb-dropdown').removeClass('filtered');
									}
								});
                            
                           
							column.data().unique().sort().each(function (d, j) {
								var // wrapped
									$label = $('<label>'),
									$text = $('<span>', {
										text: ( d && d.length > 25 )?d.substr( 0, 5 ) +'...' :d,
										title: d
									}),
									$cb = $('<input>', {
										type: 'checkbox',
										value: d,
										id: "filterId-" + d
									});

								$text.appendTo($label);
								$cb.appendTo($label);
								ddmenu.append($('<li>').append($label));
							});
                            var valueTofilter = selectedFilter;
				 			for (var m = 0; m < valueTofilter.length; m++) {
				 			    if(valueTofilter[m].length==1){
                                 $('#filterId-'+valueTofilter[m]).prop('checked',true);
				 			    }
				 			    else{
				 			        for(var k=0;k<valueTofilter[m].length;k++){
				 			            $('#filterId-'+valueTofilter[m][k]).prop('checked',true);
				 			        }
				 			    }
						}
						
                            ddmenu.children().each(function(){
                                if($('input[type="checkbox"]:checked')){
                                    
                                    widget.ob_selectedSKU([]);
									var active;
									var vals = $(':checked', ddmenu).map(function (index, element) {
										active = true;
										$(this).parent().parent().addClass('active');
										widget.ob_selectedSKU.push($(this).parent().parent().text());
										return $.fn.dataTable.util.escapeRegex($(element).val());
									}).toArray().join('|');

									column
										.search(vals.length > 0 ? '^(' + vals + ')$' : '', true, false)
										.draw();
								
									// Highlight the current filter if selected.
									var active2 = ddmenu.parent().is('.active');
									if (active && !active2) {
									    ddmenu.parent().addClass('active');
										ddmenu.parent().parent().find('.filter').text(widget.resources().obactivefilter);
										ddmenu.parent().parent().find('.caret').addClass('activefilter');
										ddmenu.parent().parent().find('.glyphicon-plus').addClass('activefilter');
										ddmenu.parent().parent().find('.filter').addClass('activefilter');
									} else if (!active && active2) {
										ddmenu.parent().removeClass('active');
										ddmenu.parent().parent().find('.filter').text(widget.resources().obfilter);
										ddmenu.parent().parent().find('.caret').removeClass('activefilter');
										ddmenu.parent().parent().find('.glyphicon-plus').removeClass('activefilter');
										ddmenu.parent().parent().find('.filter').removeClass('activefilter')
									}
									
										ddmenu.parent().prev().text(widget.ob_selectedSKU()).addClass('filteredSKU');
									if ($('input[type="checkbox"]:checked').length > 0) {
										$('.filterSKUvalues').addClass('skulist');
									} else {
										$('.filterSKUvalues').removeClass('skulist');
									}
								
                                }
                                if($('body').find('.activefilter').length > 0){
									    $('.cb-dropdown').addClass('filtered');
									}
									else{
									    $('.cb-dropdown').removeClass('filtered');
									}
                            });
                            
    			});
    			
					}

				});
				
				var table = $('#skuTable').DataTable();

				/** Clear filter functionality **/
				$('body').delegate("#clearFilter", 'click', function () {
					table.search('').columns().search('').draw();
					$('input:checkbox').removeAttr('checked');
					$('.cb-dropdown-wrap').removeClass('active');
					$('.cb-dropdown').removeClass('filtered');
					$('.cb-dropdown .active').removeClass('active');
					$('.filter').text(widget.resources().obfilter);
					$('.caret').removeClass('activefilter');
					$('.glyphicon-plus').removeClass('activefilter');
					$('.filter').removeClass('activefilter');
					$('.filterSKUvalues').text('');
					$('.filterSKUvalues').removeClass('skulist');
					widget.ob_selectedSKU([])
				});

				var wishlistItems = storageApi.getInstance().readFromCookies("obwishlistcookie");

				if (wishlistItems) {
					numberofItemsadded = JSON.parse(wishlistItems).length;
					widget.ob_wishListArray(JSON.parse(wishlistItems));
					$.Topic('addTowishlistItem.memory').publish(true, numberofItemsadded);
				}

				/** Store cookie for wishlist **/
				$('#skuTable').on('click', ".addwishlist", function () {
					var data = ko.toJS(table.row($(this).parents('tr')).data());
					for (var key in data) {
						if (key.indexOf('Ob') === -1 || data[key] === '' || data[key] === null || data[key].length === 0) {
							delete data[key];
						}
					}
					widget.ob_wishlistSKU(data,widget.product().skuProperties());
					widget.ob_wishListArray.push({
						"SKU": data,
						productname: widget.product().displayName(),
						productID: widget.product().id(),
						productmediumImageURLs: widget.product().mediumImageURLs(),
                        productURL: widget.product().route(),
                        "availableInBothCatalog": obAvailableInBothCatalog,
						"origin": widget.locale(),
						"catalog": obCatalog
					})
					var uniqueWishlist = widget.ob_wishListArray().reduce(function (item, e1) {
						var matches = item.filter(function (e2) {
							return e1.SKU['Item Number'] == e2.SKU['Item Number']
						});
						if (matches.length == 0) {
							item.push(e1);
						}
						return item;
					}, []);

					var wishlistnumberOfItems = uniqueWishlist.length;
					if(uniqueWishlist.length<=10){
					    $.Topic('addTowishlistItem.memory').publish(true, wishlistnumberOfItems);
					storageApi.getInstance().saveToCookies("obwishlistcookie", JSON.stringify(uniqueWishlist), 30);
					}
					else{
						notifier.sendError(this.WIDGET_ID, widget.translate('obexceedItemsinwishlist'));
				   }
				});
            
				/** Hide dropdown list for filter **/
				$(document).click(function (event) {

					var elementName = ($(event.target).attr("class") === "sorting_disabled") ? $(event.target).attr("class") : $(event.target).parents(".sorting_disabled").attr("class");
					var dropDownState = $(event.target).find(".cb-dropdown-wrap").is(":visible");
					$(this).find(".cb-dropdown-wrap").hide();

					if (elementName === "sorting_disabled") {
						if ($(event.target).attr("class") === "sorting_disabled") {
							if (dropDownState) {
								$(event.target).find(".cb-dropdown-wrap").hide();
							} else {
								$(event.target).find(".cb-dropdown-wrap").show();
							}
						} else {
							$(event.target).parents(".sorting_disabled").find(".cb-dropdown-wrap").show();
						}
					}
				});
			},
             /** Logic to store sku name in cookie **/
			 ob_wishlistSKU: function(data,skuproperty){
					for(var s=0;s<skuproperty.length;s++){
						if(data[skuproperty[s].id] && (skuproperty[s].id !== 'Ob_buyNowAvailable')){
							data[skuproperty[s].label] = data[skuproperty[s].id];
							delete data[skuproperty[s].id]; 
						}   
						
					}
						return data; 
            },
			beforeAppear: function (page) {
				var widget = this;
				widget.ob_wishListArray([]);
				notifier.clearError(this.WIDGET_ID);
				/** wait till Product Sku exists - Need to change the logic instead of setInterval */
				if(widget.product()) {
    				var checkSkuExist = setInterval(function () {
    					if (widget.product().skuProperties().length > 0) {
    					    clearInterval(checkSkuExist);
							widget.ob_datatable();
							
							/*** generate base64 image for Product Main image */
        				    getBase64ImageFromURL(widget.product().primaryFullImageURL()[0]).then(function(result) {
								obPdfProductImage = result;
								//console.log("obPdfProductImage..........", obPdfProductImage);
							  });
					  
							  
							  var additionalItems = widget.product().ob_packagingTypeImage();
							  var TUVIconimg = widget.product().ob_TUVIcon();
							  
							  if(TUVIconimg && (TUVIconimg.indexOf(',') !== -1)){
								 var updateTUVIconimg =  TUVIconimg.split(',');
								 for(var k=0;k<updateTUVIconimg.length;k++){
									 /*** generate base64 image for Product TUV Icon image */
									  getBase64ImageFromURL('/file/products/'+updateTUVIconimg[k]+'.jpg').then(function(result) {
										obPdfAdditionalSealImage.push(result);
									  });
								 }
							  }else if(TUVIconimg && (TUVIconimg.indexOf(',') === -1)){
								  /*** generate base64 image for Product TUV Icon image */
								  getBase64ImageFromURL('/file/products/'+TUVIconimg+'.jpg').then(function(result) {
									obPdfAdditionalSealImage.push(result);
								  });
							  }
							  
							  
							  if(additionalItems && (additionalItems.indexOf(',') > -1)) {
								  var additionalItemsImg = additionalItems.split(',');
								  for(var i=0;i<additionalItemsImg.length;i++){
									  /*** generate base64 image for Product Additional Seal image */
									getBase64ImageFromURL('/file/products/'+additionalItemsImg[i]+'.jpg').then(function(result) {
										obPdfAdditionalSealImage.push(result);
									});
								  }
							  } else if(additionalItems && (additionalItems.indexOf(',') === -1)){
								  /*** generate base64 image for Product Additional Seal image */
								  getBase64ImageFromURL('/file/products/'+additionalItems+'.jpg').then(function(result) {
									obPdfAdditionalSealImage.push(result);
								  });
							  }
					  
							  
							  
							  /*** generate base64 image for Osborn Logo */
							  getBase64ImageFromURL('/file/general/Osborn_logo.png').then(function(result) {
								obOsbornLogo = result;
								//console.log("obOsbornLogo..........", obOsbornLogo);
							  });
							  
    						var table = $('#skuTable').DataTable();
    						if (table.page.info().recordsDisplay == 0) {
    							table.search('').columns().search('').draw();
    						}
    					
    						$("#skuTable").wrap("<div class='datatable_wrapper_new'></div>");
    						
    						// logic for buynow starts
                                !function(){"use strict";var e;!function(){var e="appInsightsSDK";window[e]="channelsightTelemetry";var t=window[e],n=window[t]||function(e){function t(e){n[e]=function(){var t=arguments;n.queue.push(function(){n[e].apply(n,t)})}}var n={config:e,initialize:!0},i=document,c=window;try{n.cookie=i.cookie}catch(e){}n.queue=[],n.version=2;for(var r=["Event","PageView","Exception","Trace","DependencyData","Metric","PageViewPerformance"];r.length;)t("track"+r.pop());t("startTrackPage"),t("stopTrackPage");var o="Track"+r[0];if(t("start"+o),t("stop"+o),t("addTelemetryInitializer"),t("setAuthenticatedUserContext"),t("clearAuthenticatedUserContext"),t("flush"),!(!0===e.disableExceptionTracking||e.extensionConfig&&e.extensionConfig.ApplicationInsightsAnalytics&&!0===e.extensionConfig.ApplicationInsightsAnalytics.disableExceptionTracking)){t("_"+(r="onerror"));var s=c[r];c[r]=function(e,t,i,c,o){var a=s&&s(e,t,i,c,o);return!0!==a&&n["_"+r]({message:e,url:t,lineNumber:i,columnNumber:c,error:o}),a},e.autoExceptionInstrumented=!0}return n}({instrumentationKey:"64e0819b-102f-4031-bafb-5ccc8ddafd22",disableExceptionTracking:!0,enableDebug:!1,disableAjaxTracking:!0,disableFetchTracking:!0,disableCorrelationHeaders:!0,isCookieUseDisabled:!0,namePrefix:"channelsight"});(window[t]=n).queue&&0===n.queue.length&&n.trackPageView({})}(),document.getElementById("cswidgetjs")?(document.getElementById("cswidgetjs").remove(),(e=document.createElement("script")).src="https://cscoreproweustor.blob.core.windows.net/widget/scripts/cswidget.js",e.id="cswidgetjs",document.body.appendChild(e)):((e=document.createElement("script")).src="https://cscoreproweustor.blob.core.windows.net/widget/scripts/cswidget.js",e.id="cswidgetjs",document.body.appendChild(e))}();
                            // logic for buy now ends
    					}
                    }, 1000);
                    
                     //Catalog Check
                     widget.getProductCatalogInfo(widget.product().id());
				}

            },
            //Get Product Catalog Details
            getProductCatalogInfo: function(productID) {
            
                if(productID){
                    ccRestClient.authenticatedRequest('/ccstorex/custom/v1/productCatalogInfo', {'productID': productID}, function(success){
                        obAvailableInBothCatalog = success.availableInBothCatalog;
						obCatalog = success.catalog;
                    }, function(error){
                    }, 'POST');
                }
            }
		}  
	}
);