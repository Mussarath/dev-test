define(

  ['knockout', 'CCi18n', 'ccRestClient'],

  function(ko, CCi18n, ccRestClient) {

    "use strict";
  
    return {
        
        //Variables
        obCategories: ko.observable(''),
    
      /*
       * Note that "this" is bound to the Widget View Model.
       */      
      resourcesLoaded : function(widget) {
      },

      onLoad : function(widget) { 
      
        $(function(){
       $('[data-toggle="collapse"]').addClass("collapsed"); 
    });
       
      },

      beforeAppear : function(page) {
          
            //Initiate Categoy call
            this.obGetCategories(this);
            

      },
   
      //Category Call
      obGetCategories : function(widget) {
          var collectionID = 'obProducts';
          if(widget.locale() === "en_US" || widget.locale() === "es"){
            collectionID = 'USOBProducts';
          }
          var l = {};
          l['expand'] = 'childCategories';
          l['maxLevel'] = 1000;
          ccRestClient.authenticatedRequest("/ccstoreui/v1/collections/"+collectionID,l, function (data) {
            if(data){
                widget.obCategories(data);
              }
              }, function (err) {
          }, "GET");
        
      }

    };
  }
);


